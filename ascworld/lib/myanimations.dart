import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class MyAnimation extends StatefulWidget {
  const MyAnimation({super.key});

  @override
  State<MyAnimation> createState() => _MyAnimationState();
}

class _MyAnimationState extends State<MyAnimation> {
  double _width = 100;
  double _height = 100;
  Color _color = Colors.blue;
  bool isExpended = false;
  double _opacity = 1.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Animations')),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          // Row(
          //   children: [
          //     ElevatedButton(
          //       onPressed: () {
          //         setState(() {
          //           _width += 20;
          //           _height += 20;
          //         });
          //       },
          //       child: Text('Change Size'),
          //     ),
          //     ElevatedButton(
          //       onPressed: () {
          //         setState(() {
          //           _color = Colors.purple;
          //         });
          //       },
          //       child: Text('Change Color'),
          //     ),
          //   ],
          // ),
          Center(
            child: Column(
              children: [
                GestureDetector(
                  onTap: () {
                    setState(() {
                      _opacity = isExpended ? 1 : 0.1;
                      isExpended = !isExpended;
                    });
                  },
                  child: AnimatedOpacity(
                    opacity: _opacity,
                    duration: Duration(seconds: 3),
                    child: Container(
                      width: 200,
                      height: 200,
                      color: Colors.red,
                    ),
                  ),
                ),
                SizedBox(
                  height: 50,
                ),
                GestureDetector(
                  onTap: () {
                    _width = isExpended ? 100 : 300;
                    _height = isExpended ? 100 : 300;
                    _color = isExpended ? Colors.amber : Colors.grey;
                    isExpended = !isExpended;
                    setState(() {});
                  },
                  child: AnimatedContainer(
                    curve: Curves.bounceInOut,
                    duration: Duration(seconds: 5),
                    width: _width,
                    height: _height,
                    color: _color,
                    child: Text(isExpended ? 'Collapse' : 'Expand'),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
