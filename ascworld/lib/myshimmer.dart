import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:shimmer/shimmer.dart';

class MyShimmer extends StatefulWidget {
  const MyShimmer({super.key});

  @override
  State<MyShimmer> createState() => _MyShimmerState();
}

class _MyShimmerState extends State<MyShimmer> {
  List<String> _mylist = [];
  bool isLoaded = false;

  @override
  void initState() {
    _mylist = List.generate(5, (index) => "Index text $index");

    Future.delayed(
      Duration(seconds: 5),
      () {
        setState(() {
          isLoaded = true;
        });
      },
    );

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.all(5),
          child: !isLoaded
              ? Shimmer.fromColors(
                  baseColor: Colors.grey.shade400,
                  highlightColor: Colors.green.shade100,
                  direction: ShimmerDirection.ltr,
                  child: ListView.builder(
                    itemCount: _mylist.length,
                    itemBuilder: (context, index) {
                      return ListTile(
                        leading: CircleAvatar(),
                        title: Container(
                          width: 0.3 * MediaQuery.of(context).size.width,
                          color: Colors.grey,
                          height: 10,
                        ),
                        subtitle: Container(
                          width: 0.8 * MediaQuery.of(context).size.width,
                          color: Colors.grey,
                          height: 10,
                        ),
                      );
                    },
                  ),
                )
              : ListView.builder(
                  itemCount: _mylist.length,
                  itemBuilder: (context, index) {
                    return ListTile(
                      leading: CircleAvatar(),
                      title: Text('Title $index'),
                      subtitle: Text('Subtitme $index'),
                    );
                  },
                ),
        ),
      ),
    );
  }
}
