import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:http/http.dart' as http;

class HttpExamples extends StatefulWidget {
  const HttpExamples({super.key});

  @override
  State<HttpExamples> createState() => _HttpExamplesState();
}

class _HttpExamplesState extends State<HttpExamples> {
  List<dynamic> _data = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ElevatedButton(
                onPressed: () {
                  fetchData();
                },
                child: Text('Get'),
              ),
              ElevatedButton(
                onPressed: () {
                  insertData();
                },
                child: Text('Insert'),
              ),
              ElevatedButton(
                onPressed: () {
                  updateData();
                },
                child: Text('Update'),
              ),
              ElevatedButton(
                onPressed: () {
                  deleteData();
                },
                child: Text('Delete'),
              ),
            ],
          ),
          Expanded(
            child: ListView.builder(
              itemCount: _data.length,
              itemBuilder: (context, index) {
                return Card(
                  elevation: 10,
                  child: ListTile(
                    trailing: Text((index + 1).toString()),
                    title: Text(
                        _data[index]['title'].toString()), // Cast to String
                    subtitle:
                        Text(_data[index]['body'].toString()), // Cast to String
                  ),
                );
              },
            ),
          )
        ],
      ),
    );
  }

  Future<void> fetchData() async {
    try {
      print('started...');
      String url = "https://jsonplaceholder.typicode.com/posts";
      final response = await http.get(Uri.parse(url));
      print(response.statusCode);
      if (response.statusCode == 200) {
        // final data = response.body;
        // final data = jsonDecode(response.body);
        _data = jsonDecode(response.body);
        // for (var post in data) {}
        // _nameController.text = data[1]['title'];
        setState(() {});
        // print(data.length.toString());
      } else {
        print(response.statusCode.toString());
      }
    } catch (e) {
      print('Error occured $e');
    }
  }

  void insertData() {}

  void deleteData() {}

  void updateData() {}
}
