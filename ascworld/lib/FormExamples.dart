import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:http/http.dart' as http;
import 'package:ascworld/api_links.dart';

class FromExamples extends StatefulWidget {
  const FromExamples({super.key});

  @override
  State<FromExamples> createState() => _FromExamplesState();
}

class _FromExamplesState extends State<FromExamples> {
  final fkey = GlobalKey<FormState>();
  TextEditingController _nameController = TextEditingController();
  List<dynamic> _listviewdata = [];
  String fname = '';
  String mother = '';
  String mobile = '';
  String email = '';
  String password = '';
  String country = '';
  String gender = '';
  bool policy = false;

  Widget buildtexbox(int index, String label, String hint, IconData icon,
      TextInputType keyboard, bool isPassword) {
    return Padding(
      padding: const EdgeInsets.only(top: 10.0),
      child: TextFormField(
        // controller: _nameController,
        obscureText: isPassword,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.all(10.0),
          border: OutlineInputBorder(),
          labelText: label,
          hintText: hint,
          floatingLabelBehavior: FloatingLabelBehavior.auto,
          prefixIcon: Icon(icon),
        ),
        onSaved: (newValue) {
          if (index == 1) {
            fname = newValue ?? '';
          } else if (index == 2) {
            mother = newValue ?? '';
          } else if (index == 3) {
            mobile = newValue ?? '';
          } else if (index == 4) {
            email = newValue ?? '';
          } else if (index == 5) {
            password = newValue ?? '';
          }
        },
        validator: (value) {
          if (value!.isEmpty) {
            return '$label is required';
          }

          return null;
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Fill Registration'),
        backgroundColor: Colors.teal,
      ),
      body: Form(
        key: fkey,
        autovalidateMode: AutovalidateMode.always,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 5.0, horizontal: 30.0),
          child: ListView(
            children: [
              buildtexbox(1, 'Fullname', 'Enter your fullname',
                  Icons.person_2_outlined, TextInputType.name, false),
              buildtexbox(2, 'Mother', 'Enter your mother fullname',
                  Icons.person_2_outlined, TextInputType.name, false),
              buildtexbox(3, 'Mobile', 'Enter your mobile', Icons.phone,
                  TextInputType.phone, false),
              buildtexbox(4, 'Email', 'Enter your Email', Icons.email_outlined,
                  TextInputType.emailAddress, false),
              buildtexbox(5, 'Password', 'Enter strong password',
                  Icons.password_outlined, TextInputType.text, true),
              SizedBox(
                height: 15.0,
              ),
              DropdownButtonFormField(
                onSaved: (newValue) {
                  country = newValue ?? '';
                },
                value: country,
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Country is required';
                  }
                  return null;
                },
                items: [
                  DropdownMenuItem(
                    child: Text('Select'),
                    value: '',
                  ),
                  DropdownMenuItem(
                    child: Text('Somaliland'),
                    value: 'sl',
                  ),
                  DropdownMenuItem(
                    child: Text('Ethiopia'),
                    value: 'et',
                  ),
                  DropdownMenuItem(
                    child: Text('Kenya'),
                    value: 'ke',
                  ),
                ],
                onChanged: (value) {
                  setState(() {
                    country = value ?? '';
                  });
                },
                decoration: InputDecoration(
                  labelText: 'Country',
                  prefixIcon: Icon(Icons.map_outlined),
                  border: OutlineInputBorder(),
                ),
              ),
              SizedBox(
                height: 15.0,
              ),
              Row(
                children: [
                  Text(
                    'Gender:',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  Radio(
                    value: 'male',
                    groupValue: gender,
                    onChanged: (value) {
                      setState(() {
                        gender = value ?? '';
                      });
                    },
                  ),
                  Text('Male'),
                  SizedBox(
                    width: 50.0,
                  ),
                  Radio(
                    value: 'female',
                    groupValue: gender,
                    onChanged: (value) {
                      setState(() {
                        gender = value ?? '';
                      });
                    },
                  ),
                  Text('Female'),
                ],
              ),
              SizedBox(
                height: 15.0,
              ),
              CheckboxListTile(
                title: Text(
                  'Agree Policy',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                subtitle: Text(
                    'I hereby confirm that I accepted all terms and conditions that apply to the use of this software. including distribution rights'),
                value: policy,
                onChanged: (value) {
                  setState(() {
                    policy = value ?? false;
                  });
                },
              ),
              SizedBox(
                height: 15.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  ElevatedButton(
                      onPressed: () async {
                        //display confirmation box
                        showDialog(
                          context: context,
                          builder: (context) {
                            return AlertDialog(
                              title: Text('Confirmation'),
                              content: Text('Are you sure you want to save?'),
                              actions: [
                                TextButton(
                                  onPressed: () {
                                    Navigator.of(context).pop(false);
                                  },
                                  child: Text('No'),
                                ),
                                TextButton(
                                  onPressed: () {
                                    Navigator.of(context).pop(true);
                                  },
                                  child: Text('Yes'),
                                ),
                              ],
                            );
                          },
                        ).then((value) async {
                          if (value) {
                            // to validate the fields
                            if (fkey.currentState!.validate()) {
                              fkey.currentState!.save();

                              //prepare the object
                              Map<String, dynamic> newobj = {
                                "fullname": fname,
                                "mother": mother,
                                "mobile": mobile,
                                "email": email,
                                "gender": gender,
                                "country": country,
                                "is_policyaccepted": policy,
                                "password": password
                              };

                              var jsonString = jsonEncode(newobj);
                              print(jsonString);
                              var resp = await http.post(
                                  Uri.parse(api_links.create_url),
                                  headers: {'content-type': 'application/json'},
                                  body: jsonString);

                              print(resp.body);

                              if (resp.statusCode == 200) {
                                ScaffoldMessenger.of(context).showSnackBar(
                                  SnackBar(
                                    content: Text(
                                        'Success fully created employee !!'),
                                    duration: Duration(seconds: 4),
                                    backgroundColor: Colors.blue,
                                  ),
                                );

                                Future.delayed(
                                  Duration(seconds: 2),
                                  () {
                                    Navigator.of(context).pop();
                                  },
                                );
                              } else {
                                String _e = resp.body;
                                ScaffoldMessenger.of(context).showSnackBar(
                                  SnackBar(
                                    content: Text('Error, $_e'),
                                    duration: Duration(seconds: 4),
                                    backgroundColor: Colors.red,
                                  ),
                                );
                              }

                              //send the to api
                            }
                          }
                        });
                      },
                      child: Row(
                        children: [
                          Icon(Icons.send_outlined),
                          SizedBox(
                            width: 15.0,
                          ),
                          Text('Submit Data'),
                        ],
                      )),
                ],
              ),
              // Row(
              //   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              //   children: [
              //     ElevatedButton(
              //       onPressed: () {
              //         fetchData();
              //       },
              //       child: Text('Get'),
              //     ),
              //     ElevatedButton(
              //       onPressed: () {
              //         insertData();
              //       },
              //       child: Text('Insert'),
              //     ),
              //     ElevatedButton(
              //       onPressed: () {
              //         updateData();
              //       },
              //       child: Text('Update'),
              //     ),
              //     ElevatedButton(
              //       onPressed: () {
              //         deleteData();
              //       },
              //       child: Text('Delete'),
              //     ),
              //   ],
              // ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> fetchData() async {
    try {
      String url = "https://jsonplaceholder.typicode.com/posts";
      final response = await http.get(Uri.parse(url));
      if (response.statusCode == 200) {
        // final data = response.body;
        // final data = jsonDecode(response.body);
        _listviewdata = jsonDecode(response.body);
        // for (var post in data) {}
        // _nameController.text = data[1]['title'];
        setState(() {});
        // print(data.length.toString());
      } else {
        print(response.statusCode.toString());
      }
    } catch (e) {
      print('Error occured $e');
    }
  }

  void updateData() {}

  void insertData() {}

  void deleteData() {}
}
