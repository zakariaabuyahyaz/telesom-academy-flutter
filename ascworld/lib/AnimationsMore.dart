import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class AnimationsMore extends StatefulWidget {
  const AnimationsMore({super.key});

  @override
  State<AnimationsMore> createState() => _AnimationsMoreState();
}

class _AnimationsMoreState extends State<AnimationsMore> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.indigo,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
      ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TweenAnimationBuilder(
              duration: Duration(seconds: 4),
              tween: Tween<double>(begin: 0.4, end: 1),
              builder: (context, value, _) {
                return Opacity(
                  opacity: value,
                  child: Padding(
                    padding: EdgeInsets.only(left: 100 * value),
                    child: Text(
                      'Flutter Animations',
                      style: TextStyle(
                          fontSize: 30 * value,
                          letterSpacing: 1,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                    ),
                  ),
                );
              },
            ),
            TweenAnimationBuilder(
              duration: Duration(seconds: 4),
              tween: ColorTween(begin: Colors.white, end: Colors.orange),
              builder: (context, value, _) {
                return Text(
                  'Flutter Animations',
                  style: TextStyle(
                      fontSize: 30,
                      letterSpacing: 1,
                      fontWeight: FontWeight.bold,
                      color: value),
                );
              },
            ),
            SizedBox(
              height: 50,
            ),
            Text(
              'This is paragraph, This is paragraph, This is paragraph, This is paragraph, This is paragraph, This is paragraph, This is paragraph, This is paragraph, ',
              style: TextStyle(
                  fontSize: 12, letterSpacing: 1, color: Colors.white),
            ),
          ],
        ),
      ),
    );
  }
}
