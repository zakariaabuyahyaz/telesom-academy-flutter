import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class MediaExamples extends StatefulWidget {
  const MediaExamples({super.key});

  @override
  State<MediaExamples> createState() => _MediaExamplesState();
}

class _MediaExamplesState extends State<MediaExamples> {
  late int counter = 0;

  @override
  void initState() {
    print('Init state called');
    counter = 12;

    // TODO: implement initState
    super.initState();
  }

  MyScreenSize getScreenSize(BuildContext context) {
    Size screen = MediaQuery.of(context).size;
    if (screen.width <= 360) {
      return MyScreenSize.small;
    } else if (screen.width > 360 && screen.width < 700) {
      return MyScreenSize.medium;
    } else {
      return MyScreenSize.large;
    }
  }

  @override
  Widget build(BuildContext context) {
    print('build is called...');
    Orientation orientation = MediaQuery.of(context).orientation;
    final dpi =
        MediaQuery.of(context).devicePixelRatio; //physical - logical ratio

    Size screen = MediaQuery.of(context).size;
    int colcount = 1;
    // MyScreenSize msize = getScreenSize(context);
    // if (msize == MyScreenSize.medium) {
    //   colcount = 3;
    // } else if (msize == MyScreenSize.large) {
    //   colcount = 5;
    // }

    if (orientation == Orientation.portrait) {
      colcount = 1;
    } else {
      colcount = 4;
    }

    return Scaffold(
      appBar: AppBar(
        title: Text("Media Examples " + dpi.toString()),
      ),
      body: Center(
        child: GridView.builder(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: colcount,
              crossAxisSpacing: 1.0,
              mainAxisSpacing: 1.0),
          itemCount: 10,
          itemBuilder: (context, index) {
            return Container(
              color: Colors.purple,
            );
          },
        ),
      ),
    );
  }
}

enum MyScreenSize { small, medium, large }
