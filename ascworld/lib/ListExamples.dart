import 'package:ascworld/MyGrid.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:ascworld/PhoneCustomer.dart';

class MyList extends StatefulWidget {
  const MyList({super.key});

  @override
  State<MyList> createState() => _MyListState();
}

class _MyListState extends State<MyList> {
  final List<String> ourlist =
      List.generate(100, (index) => 'Item Created $index');

  final List<PhonebookCustomer> mybook = [
    PhonebookCustomer(
        mobile: '0634445566',
        Email: 'ahmed@gmail.com',
        Name: 'Ahmed Jama',
        Image: 'assets/images/img1.png'),
    PhonebookCustomer(
        mobile: '0634781010',
        Email: 'omer@gmail.com',
        Name: 'Omer Jama',
        Image: 'assets/images/img1.png'),
    PhonebookCustomer(
        mobile: '0634445566',
        Email: 'ahmed@gmail.com',
        Name: 'Ahmed Jama',
        Image: 'assets/images/img1.png'),
    PhonebookCustomer(
        mobile: '0634420000',
        Email: 'ahmed@gmail.com',
        Name: 'Ahmed Jama',
        Image: 'assets/images/img1.png'),
    PhonebookCustomer(
        mobile: '0637789966',
        Email: 'ahmed@gmail.com',
        Name: 'Ahmed Jama',
        Image: 'assets/images/img1.png'),
    PhonebookCustomer(
        mobile: '0631020302',
        Email: 'Khalid@gmail.com',
        Name: 'Khalid Jama',
        Image: 'assets/images/img1.png'),
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(title: Text("List Examples")),
        body: ListView.separated(
          itemCount: mybook.length,
          itemBuilder: (context, index) {
            PhonebookCustomer _customer = mybook[index];
            return ListTile(
              title: Text(
                _customer.Name,
                style: TextStyle(fontSize: 20.0),
              ),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(_customer.mobile),
                  Text(_customer.Email),
                ],
              ),
              trailing: PopupMenuButton(
                itemBuilder: (context) {
                  return [
                    PopupMenuItem(child: Text('Delete'), value: 'd'),
                    PopupMenuItem(child: Text('Share'), value: 's'),
                  ];
                },
                onSelected: (value) {
                  if (value == 'd') {
                    mybook.remove(_customer);
                    setState(() {});
                    print('you are deleted');
                  } else {
                    print('Other options');

                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => MyGrid(),
                      ),
                    );
                  }
                },
              ),
            );
          },
          separatorBuilder: (context, index) {
            return Divider(
              color: Colors.grey.shade200,
              thickness: 1,
            );
          },
        ),
        // body: ListView.separated(
        //   padding: EdgeInsets.all(5.0),
        //   itemCount: ourlist.length,
        //   itemBuilder: (context, index) {
        //     return ListTile(
        //       leading: Text(
        //         index.toString(),
        //         style: TextStyle(fontSize: 15.0),
        //       ),
        //       title: Text(
        //         'Ahmed Ali Jama',
        //         style: TextStyle(fontSize: 20.0),
        //       ),
        //       subtitle: Column(
        //         crossAxisAlignment: CrossAxisAlignment.start,
        //         children: [
        //           Text('252634585520'),
        //           Text('ahmed@gmail.com'),
        //         ],
        //       ),
        //       trailing: Icon(Icons.image),
        //     );
        //   },
        //   separatorBuilder: (context, index) {
        //     return Divider(
        //       color: Colors.red,
        //       thickness: 2,
        //     );
        //   },
        // ),
        // body: Center(
        //   child: ListTile(
        //     leading: Icon(Icons.mobile_screen_share_outlined),
        //     title: Text(
        //       'Telesom Academy',
        //       style: TextStyle(fontSize: 23.0),
        //     ),
        //     subtitle: Text(
        //         'Telesom Academy provides courses to student. like flutter, web dev and others please contact to 522222'),
        //     trailing: Icon(Icons.menu_outlined),
        //     onTap: () {
        //       //do something
        //       print('It is clicked');
        //     },
        //   ),
        // ),
      ),
    );
  }
}
