import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class LTHome extends StatefulWidget {
  const LTHome({super.key});

  @override
  State<LTHome> createState() => _LTHomeState();
}

class _LTHomeState extends State<LTHome> {
  late List<Widget> _allscreens;
  int _sindex = 0;

  List<BottomNavigationBarItem> _getnavigationItems() {
    return [
      BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home'),
      BottomNavigationBarItem(
          icon: Icon(Icons.currency_rupee_outlined), label: 'Transactions'),
      BottomNavigationBarItem(icon: Icon(Icons.person), label: 'Profile'),
    ];
  }

  Widget returnHome() {
    return Center(
      child: Text('Home'),
    );
  }

  Widget returnProfile() {
    return Center(
      child: Text('User Profile'),
    );
  }

  Widget returnTransaction() {
    return Center(
      child: Text('Last Transactions'),
    );
  }

  @override
  void initState() {
    _allscreens = [returnHome(), returnTransaction(), returnProfile()];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Bottom Navigation')),
      body: _allscreens[_sindex],
      bottomNavigationBar: BottomNavigationBar(
        items: _getnavigationItems(),
        selectedItemColor: Colors.blue,
        currentIndex: _sindex,
        onTap: (value) {
          setState(() {
            _sindex = value;
          });
        },
      ),
    );
  }
}
