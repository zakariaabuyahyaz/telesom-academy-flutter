import 'package:flutter/material.dart';

final MyOrangeTheme = ThemeData(
  brightness: Brightness.light,
  primaryColor: Colors.orange.shade400,
  accentColor: Colors.orange.shade200,
  iconTheme: IconThemeData(color: Colors.orange.shade400, size: 15),
  textTheme: TextTheme(
    titleLarge: TextStyle(
        fontSize: 25.0, fontWeight: FontWeight.bold, color: Colors.black),
    bodySmall: TextStyle(
        fontSize: 15.0, fontWeight: FontWeight.normal, color: Colors.black45),
  ),
  elevatedButtonTheme: ElevatedButtonThemeData(
    style: ElevatedButton.styleFrom(
      primary: Colors.orange.shade400,
      onPrimary: Colors.black,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
    ),
  ),
);
final MyPurpleTheme = ThemeData(
  brightness: Brightness.light,
  primaryColor: Colors.purple.shade400,
  accentColor: Colors.purple.shade200,
  iconTheme: IconThemeData(color: Colors.purple.shade400, size: 50),
  textTheme: TextTheme(
    titleLarge: TextStyle(
        fontSize: 25.0, fontWeight: FontWeight.bold, color: Colors.black),
    bodySmall: TextStyle(
        fontSize: 15.0,
        fontWeight: FontWeight.normal,
        color: Colors.purple.shade200),
  ),
  elevatedButtonTheme: ElevatedButtonThemeData(
    style: ElevatedButton.styleFrom(
      primary: Colors.purple.shade400,
      onPrimary: Colors.white,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
    ),
  ),
);
final MyDarkTheme = ThemeData(brightness: Brightness.dark);
