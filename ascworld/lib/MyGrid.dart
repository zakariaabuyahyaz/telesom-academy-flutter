import 'package:ascworld/PhoneCustomer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class MyGrid extends StatefulWidget {
  const MyGrid({super.key});

  @override
  State<MyGrid> createState() => _MyGridState();
}

class _MyGridState extends State<MyGrid> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Examples"),
        ),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: Stack(
                  children: [
                    Container(
                      width: double.infinity,
                      color: Colors.purple[300],
                    ),
                    Align(
                      alignment: Alignment(0, 3),
                      child: CircleAvatar(
                        radius: 70,
                        backgroundColor: Colors.blue.shade100,
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                flex: 5,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 100,
                    ),
                    Text(
                      'Ahmed Dahir Jama',
                      style: TextStyle(fontSize: 25.0),
                    ),
                    Text(
                      'Ahmed is new user in this app. new user in this app. new user in this app. new user in this app.  ',
                      style: TextStyle(fontSize: 10.0),
                    ),
                  ],
                ),
              ),
              Expanded(
                flex: 1,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    IconButton(
                        onPressed: () {
                          print('xxxxxx');
                        },
                        icon: Icon(Icons.share)),
                  ],
                ),
              )
            ],
          ),
        ),
        // return MaterialApp(
        //   home: Scaffold(
        //     appBar: AppBar(title: Text("Grid Examples")),
        //     body: GridView.builder(
        //       gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        //           crossAxisCount: 4, crossAxisSpacing: 5.0, mainAxisSpacing: 5.0),
        //       itemCount: 10,
        //       itemBuilder: (context, index) {
        //         int alpha = index * 100;
        //         int red = index * 100;
        //         int green = index * 10;
        //         int blue = index * 100;
        //         return Container(
        //           color: Color.fromARGB(alpha, red, green, blue),
        //           child: Text('Item index $index'),
        //         );
        //       },
        //     ),
        // body: GridView(
        //   gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        //       crossAxisCount: 1, crossAxisSpacing: 5.0, mainAxisSpacing: 5.0),
        //   children: [
        //     Container(
        //       color: Colors.purple,
        //     ),
        //     Container(
        //       color: Colors.purple,
        //     ),
        //     Container(
        //       color: Colors.purple,
        //     ),
        //     Container(
        //       color: Colors.purple,
        //     ),
        //     Container(
        //       color: Colors.purple,
        //     ),
        //     Container(
        //       color: Colors.purple,
        //     ),
        //   ],
        // ),
      ),
    );
  }
}
