import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ThemeExamples extends StatefulWidget {
  const ThemeExamples({super.key});

  @override
  State<ThemeExamples> createState() => _ThemeExamplesState();
}

class _ThemeExamplesState extends State<ThemeExamples> {
  TextEditingController _utext = TextEditingController();
  String uname = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        title: Text('Example'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListView(
          children: [
            Icon(
              color: Theme.of(context).primaryColor,
              Icons.app_registration_outlined,
              size: 35.0,
            ),
            SizedBox(
              height: 30.0,
            ),
            TextFormField(
              controller: _utext,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                prefixIcon: Icon(Icons.person),
                hintText: 'Enter Fullname',
                label: Text(
                  'Fullname',
                  style: TextStyle(color: Theme.of(context).primaryColor),
                ),
              ),
            ),
            SizedBox(
              height: 30.0,
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                  primary: Theme.of(context).primaryColor),
              onPressed: () {},
              child: Row(
                children: [
                  Icon(Icons.send),
                  SizedBox(
                    width: 10.0,
                  ),
                  Text('Submit'),
                ],
              ),
            ),
            SizedBox(
              height: 50.0,
            ),
            Text(
              'The End',
              style: TextStyle(
                  fontSize: 30.0, color: Theme.of(context).primaryColor),
            ),
            Divider(
              height: 15.0,
              color: Theme.of(context).primaryColor,
            ),
            SizedBox(
              height: 20.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      primary: Theme.of(context).primaryColor),
                  onPressed: () async {
                    final pref = await SharedPreferences.getInstance();
                    uname = pref.getString("username") ?? 'Null';
                    setState(() {});
                  },
                  child: Row(
                    children: [
                      Text('Read'),
                    ],
                  ),
                ),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      primary: Theme.of(context).primaryColor),
                  onPressed: () async {
                    final pref = await SharedPreferences.getInstance();
                    pref.setString("username", _utext.text);
                    setState(() {
                      uname = _utext.text;
                    });
                  },
                  child: Row(
                    children: [
                      Text('Store'),
                    ],
                  ),
                ),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      primary: Theme.of(context).primaryColor),
                  onPressed: () async {
                    final pref = await SharedPreferences.getInstance();
                    pref.remove("username");
                    uname = "";
                    setState(() {});
                  },
                  child: Row(
                    children: [
                      Text('Remove'),
                    ],
                  ),
                ),
              ],
            ),
            Text(
              'Welcome $uname',
              style: TextStyle(
                  fontSize: 30.0, color: Theme.of(context).primaryColor),
            ),
          ],
        ),
      ),
    );
  }
}

//     return Scaffold(
//       appBar: AppBar(
//         backgroundColor: Theme.of(context).primaryColor,
//         title: Text(
//           'Example',
//           style: Theme.of(context).textTheme.titleLarge,
//         ),
//       ),
//       body: Padding(
//         padding: const EdgeInsets.all(8.0),
//         child: ListView(
//           children: [
//             Icon(
//               // color: Theme.of(context).primaryColor,
//               Icons.app_registration_outlined,
//               // size: 35.0,
//             ),
//             SizedBox(
//               height: 30.0,
//             ),
//             TextFormField(
//               decoration: InputDecoration(
//                 border: OutlineInputBorder(),
//                 prefixIcon: Icon(Icons.person),
//                 hintText: 'Enter Fullname',
//                 label: Text(
//                   'Fullname',
//                   style: TextStyle(color: Theme.of(context).primaryColor),
//                 ),
//               ),
//             ),
//             ElevatedButton(
//               // style: ElevatedButton.styleFrom(
//               //     primary: Theme.of(context).primaryColor),
//               onPressed: () {},
//               child: Row(
//                 children: [
//                   Icon(Icons.send),
//                   SizedBox(
//                     width: 10.0,
//                   ),
//                   Text('Submit'),
//                 ],
//               ),
//             ),
//             SizedBox(
//               height: 50.0,
//             ),
//             Text(
//               'The End',
//               style: Theme.of(context).textTheme.titleLarge,
//             ),
//             Text(
//               'Today we finished theme tutorials Today we finished theme tutorials Today we finished theme tutorials',
//               style: Theme.of(context).textTheme.bodySmall,
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }





    