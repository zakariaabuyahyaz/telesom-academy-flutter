import 'package:ascworld/RDrawer.dart';
import 'package:ascworld/Rdetails.dart';
import 'package:ascworld/Rsettings.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class RHome extends StatefulWidget {
  const RHome({super.key});

  @override
  State<RHome> createState() => _RHomeState();
}

class _RHomeState extends State<RHome> {
  final GlobalKey<ScaffoldState> _skey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _skey,
      drawer: Drawer(
        backgroundColor: Colors.white,
        width: MediaQuery.of(context).size.width * 0.5,
        child: RDrawer(),
      ),
      appBar: AppBar(
        title: Text('Home'),
        // leading: Icon(Icons.home),
      ),
      body: Padding(
        padding: EdgeInsets.all(20.0),
        child: Column(
          children: [
            ElevatedButton(
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => RSettings(),
                  ));

                  // Navigator.push(
                  //     context,
                  //     MaterialPageRoute(
                  //       builder: (context) => RSettings(),
                  //     ));
                },
                child: Text('Settings')),
            ElevatedButton(
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => RDetails(Name: 'Mobile S15'),
                  ));
                },
                child: Text('Details')),
            ElevatedButton(
                onPressed: () {
                  _skey.currentState!.openDrawer();
                },
                child: Text('Open drawer')),
          ],
        ),
      ),
    );
  }
}
