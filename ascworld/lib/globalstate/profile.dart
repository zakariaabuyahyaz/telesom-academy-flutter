import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ascworld/globalstate/MyAppState.dart';

class ProfileScreen extends StatelessWidget {
  final AppState globalstate = Get.find<AppState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Profile Screen'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Obx(() => Text('User ID: ${globalstate.userId.value}')),
            Obx(() => Text('Name: ${globalstate.name.value}')),
            Obx(() => Text('Theme: ${globalstate.themeName.value}')),
            SizedBox(height: 20),
            ElevatedButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text('Back to Home'),
            ),
          ],
        ),
      ),
    );
  }
}
