import 'package:get/get.dart';

class flutterstudent {
  late int id;
  late String name;

  flutterstudent() {
    this.id = 0;
    this.name = "";
  }
}

class AppState extends GetxController {
  var userId = ''.obs;
  var name = ''.obs;
  var themeName = 'light'.obs;
  Rx<flutterstudent> fs = flutterstudent().obs;

  void updateUser(String id, String newName) {
    userId.value = id;
    name.value = newName;
  }

  void updateTheme(String newtheme) {
    themeName.value = newtheme;
  }
}
