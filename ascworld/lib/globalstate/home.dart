import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ascworld/globalstate/MyAppState.dart';
import 'package:ascworld/globalstate/profile.dart';

class HomeScreen extends StatelessWidget {
  final AppState globalstate = Get.find<AppState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home Screen'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Obx(() => Text('User ID: ${globalstate.userId.value}')),
            Obx(() => Text('Name: ${globalstate.name.value}')),
            Obx(() => Text('Theme: ${globalstate.themeName.value}')),
            SizedBox(height: 20),
            ElevatedButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => ProfileScreen(),
                ));
              },
              child: Text('Go to Profile'),
            ),
            ElevatedButton(
              onPressed: () {
                globalstate.updateUser('123', 'Abdi jama');
              },
              child: Text('Update User Data'),
            ),
            ElevatedButton(
              onPressed: () {
                globalstate.updateTheme(
                    globalstate.themeName.value == "light" ? "dark" : "light");
              },
              child: Obx(() {
                String newtheme =
                    globalstate.themeName.value == "light" ? "DARK" : "LIGHT";
                return Text('Change Them to $newtheme');
              }),
            ),
          ],
        ),
      ),
    );
  }
}
