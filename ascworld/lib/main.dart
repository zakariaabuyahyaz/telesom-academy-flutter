import 'package:ascworld/AnimationsMore.dart';
import 'package:ascworld/Backend/list.dart';
import 'package:ascworld/FormExamples.dart';
import 'package:ascworld/MediaExamples.dart';
import 'package:ascworld/MyGrid.dart';
import 'package:ascworld/RHome.dart';
import 'package:ascworld/Rdetails.dart';
import 'package:ascworld/Rsettings.dart';
import 'package:ascworld/employeelist.dart';
import 'package:ascworld/myanimations.dart';
import 'package:ascworld/myshimmer.dart';
import 'package:ascworld/tabs/THome.dart';
import 'package:ascworld/taskapp/tasklist.dart';
import 'package:ascworld/theme/MyTheme.dart';
import 'package:ascworld/themeexample.dart';
import 'package:flutter/material.dart';
import 'package:ascworld/ExampleOne.dart';
import 'package:ascworld/Exampletwo.dart';
import 'package:ascworld/ExampleMap.dart';
import 'package:ascworld/mycards.dart';
import 'package:ascworld/ListExamples.dart';
import 'package:ascworld/globalstate/MyAppState.dart';
import 'package:get/get.dart';
import 'package:ascworld/globalstate/home.dart';

void main() {
  //is where the project starts executing
  //initialize and register the state
  Get.put(AppState());
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final globalState = Get.find<AppState>();
  MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Obx(() {
      ThemeMode _ctheme = globalState.themeName.value == "light"
          ? ThemeMode.light
          : ThemeMode.dark;

      return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: MyPurpleTheme,
        themeMode: _ctheme,
        darkTheme: MyDarkTheme,
        home: HomeScreen(),

        // initialRoute: "/",
        // routes: {
        //   '/': (context) => RHome(),
        //   '/settings': (context) => RSettings(),
        //   '/details': (context) {
        //     //write code
        //     var s = 'Test Product';
        //     return RDetails(Name: s);
        //   }
        // },
      );
    });
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headlineMedium,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

class ExampleOne_OLD extends StatelessWidget {
  const ExampleOne_OLD({super.key});

  @override
  Widget build(BuildContext context) {
    //build is responsible for building user interface
    return Scaffold(
      appBar: AppBar(
        title: Text('Example One Updated'),
        backgroundColor: Colors.orange,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [Text('One')],
          ),
        ],
      ),
      // body: Column(
      //   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      //   children: [
      //     Container(
      //       decoration: BoxDecoration(
      //         color: Colors.red,
      //         border: Border.all(width: 5, color: Colors.black),
      //         borderRadius: BorderRadius.circular(20),
      //       ),
      //       alignment: Alignment.bottomRight,
      //       width: 200,
      //       height: 200,
      //       child: Text('this is child'),
      //     ),
      //     Text('One'),
      //     Row(
      //       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      //       children: [
      //         Text('1'),
      //         Text('2'),
      //         Text('3'),
      //       ],
      //     ),
      //     Text('Two'),
      //     Text('Three'),
      //   ],
      // ),
    );
  }
}

class ExampleThree extends StatefulWidget {
  const ExampleThree({super.key});

  @override
  State<ExampleThree> createState() => _ExampleThreeState();
}

class _ExampleThreeState extends State<ExampleThree> {
  bool isshown = true;
  String data = "Data is shown";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Example Three',
          style: TextStyle(
              color: Colors.black,
              decoration: TextDecoration.underline,
              backgroundColor: Colors.white,
              fontSize: 30,
              fontWeight: FontWeight.bold,
              fontStyle: FontStyle.italic),
        ),
      ),
      body: Column(
        children: [
          Text(
            'Nested Widgets',
            style: TextStyle(
              color: Colors.red,
              fontSize: 30,
              decoration: TextDecoration.underline,
            ),
          ),
          Stack(
            children: [
              Container(
                height: 200,
                width: 200,
                color: Colors.blue,
              ),
              Positioned(
                left: 20,
                top: 30,
                child: Container(
                  width: 100,
                  height: 100,
                  color: Colors.amber[200],
                ),
              ),
              Positioned(
                left: 70,
                top: 70,
                child: Text(
                  'Stack Widget',
                ),
              ),
            ],
          ),
          ElevatedButton(
            style: ElevatedButton.styleFrom(
              backgroundColor: Colors.black,
              elevation: 2,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
            ),
            onPressed: () {
              //dart code
              int x = 90;
              int b = 10;
              int c = x + b;
              print('you clicked me, $c');
            },
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Icon(
                  Icons.mail_outline_outlined,
                  color: Colors.white,
                ),
                Text('Send Email')
              ],
            ),
          ),
          Column(
            children: [
              if (isshown) Text('This is your content:  $data'),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ElevatedButton(
                      onPressed: () {
                        setState(() {
                          isshown = true;
                        });
                      },
                      child: Text('Show')),
                  ElevatedButton(
                      onPressed: () {
                        setState(() {
                          isshown = false;
                        });
                      },
                      child: Text('Hide')),
                ],
              ),
            ],
          ),
          Text('Agreement: do you agree this terms?'),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              TextButton(
                  onPressed: () {
                    setState(() {
                      isshown = true;
                    });
                  },
                  child: Text('Yes')),
              TextButton(
                  onPressed: () {
                    setState(() {
                      isshown = false;
                    });
                  },
                  child: Text('No')),
            ],
          ),
        ],
      ),
    );
  }
}

class Exercise extends StatefulWidget {
  const Exercise({super.key});

  @override
  State<Exercise> createState() => _ExerciseState();
}

class _ExerciseState extends State<Exercise> {
  Widget getCard(String title, IconData icon, Color color) {
    return Container(
      margin: EdgeInsets.all(10),
      alignment: Alignment.center,
      width: 100,
      height: 100,
      color: color,
      child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        Icon(icon),
        Text(title),
      ]),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Exercice'),
      ),
      body: Column(
        children: [
          getCard('Home', Icons.home_outlined, Colors.red),
          getCard('Repairing', Icons.home_repair_service_outlined, Colors.blue),
          getCard('Car', Icons.car_repair_outlined, Colors.green)
        ],
      ),
    );
  }
}
