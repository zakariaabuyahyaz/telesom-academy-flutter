class Question {
  String text;
  List<Answer> answers;

  Question({required this.text, required this.answers});
}

class Answer {
  String text;
  bool isCorrect;
  int Score;
  Answer({required this.text, required this.isCorrect, required this.Score});
}
