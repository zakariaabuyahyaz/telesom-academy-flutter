import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class RDetails extends StatelessWidget {
  String Name;
  RDetails({super.key, required this.Name});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Details'),
        // leading: Icon(Icons.settings),
      ),
      body: Column(
        children: [
          Text('Name: ' + Name),
          ElevatedButton(
            onPressed: () {
              Navigator.of(context).pop();
              // Navigator.pop(context);
            },
            child: Text('Go Back'),
          ),
        ],
      ),
    );
  }
}
