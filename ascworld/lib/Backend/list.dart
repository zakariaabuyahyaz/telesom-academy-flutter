import 'dart:convert';

import 'package:ascworld/api_links.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:http/http.dart' as http;

class BListEmployee extends StatefulWidget {
  const BListEmployee({super.key});

  @override
  State<BListEmployee> createState() => _BListEmployeeState();
}

class _BListEmployeeState extends State<BListEmployee> {
  late List<dynamic> _empdata;

  Future<void> fetch_Data() async {
    var response = await http.get(Uri.parse(api_links.fetch_url),
        headers: {'content-type': 'application/json'});
    if (response.statusCode == 200) {
      _empdata = jsonDecode(response.body);
    } else {
      _empdata = [];
    }
  }

  @override
  void initState() {
    fetch_Data().then(
      (value) {
        setState(() {});
      },
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Employee List')),
      body: Padding(
        padding: EdgeInsets.all(15),
        child: RefreshIndicator(
          onRefresh: () async {
            fetch_Data().then(
              (value) {
                setState(() {});
              },
            );
          },
          child: ListView.builder(
            itemCount: _empdata.length,
            itemBuilder: (context, index) {
              var emp = _empdata[index];
              return ListTile(
                title: Text(emp['fullname']),
              );
            },
          ),
        ),
      ),
    );
  }
}
