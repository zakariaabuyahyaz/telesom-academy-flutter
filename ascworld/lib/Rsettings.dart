import 'package:ascworld/Rdetails.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class RSettings extends StatelessWidget {
  const RSettings({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Settings'),
        // leading: Icon(Icons.settings),
      ),
      body: Column(
        children: [
          Text('Welcome to Settings screen'),
          ElevatedButton(
            onPressed: () {
              Navigator.of(context).pop();
              // Navigator.pop(context);
            },
            child: Text('Go Back'),
          ),
          ElevatedButton(
              onPressed: () {
                Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (context) => RDetails(Name: 'From Settings'),
                ));
              },
              child: Text('Details')),
        ],
      ),
    );
  }
}
