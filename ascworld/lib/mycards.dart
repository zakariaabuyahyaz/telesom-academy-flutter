import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flip_card/flip_card.dart';
import 'package:ascworld/questions.dart';

class MyCards extends StatefulWidget {
  const MyCards({super.key});

  @override
  State<MyCards> createState() => _MyCardsState();
}

class _MyCardsState extends State<MyCards> {
  int cindex = 0;
  bool _must_flip = false;
  String _c_answer = "";
  int _u_score = 0;
  bool respond_allowed = true;
  late List<Question> _questions = getQuestions();

  List<Question> getQuestions() {
    List<Question> data = [];
    data.add(Question(text: "Sheeg Caasimada Somaliland", answers: [
      Answer(text: "Boorama", isCorrect: false, Score: 0),
      Answer(text: "Hargeisa", isCorrect: true, Score: 1),
      Answer(text: "Burco", isCorrect: false, Score: 0)
    ]));

    data.add(Question(text: "tirada suurada quranka sheeg", answers: [
      Answer(text: "100 suuradood", isCorrect: false, Score: 0),
      Answer(text: "114 suuradood", isCorrect: true, Score: 1),
      Answer(text: "110 suuradood", isCorrect: false, Score: 0)
    ]));

    data.add(Question(text: "Waa maxey Flutter", answers: [
      Answer(text: "UI Kit", isCorrect: false, Score: 0),
      Answer(text: "Framework", isCorrect: true, Score: 1),
      Answer(text: "None", isCorrect: false, Score: 0)
    ]));

    return data;
  }

  String getCorrectAnswer(Question q) {
    for (var i = 0; i < q.answers.length; i++) {
      if (q.answers[i].isCorrect) {
        return q.answers[i].text;
      }
    }

    return "Not Found";
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(title: Text("Example")),
        body: Column(
          children: [
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                OutlinedButton(
                  onPressed: () {
                    if (cindex > 0) {
                      setState(() {
                        _must_flip = false;
                        cindex--;
                      });
                    }
                  },
                  child: Text('BACK'),
                ),
                Text(
                  '$_u_score',
                  style: TextStyle(fontSize: 25.0),
                ),
                OutlinedButton(
                  onPressed: () {
                    if (cindex < _questions.length - 1) {
                      setState(() {
                        respond_allowed = true;
                        _must_flip = false;
                        cindex++;
                      });
                    }
                  },
                  child: Text('NEXT'),
                ),
              ],
            ),
            FlipCard(
              direction: FlipDirection.VERTICAL,
              flipOnTouch: _must_flip,
              front: Container(
                margin: EdgeInsets.all(5),
                width: double.infinity,
                height: 150,
                color: Colors.blue.shade600,
                child: Center(
                    child: Text(
                  _questions[cindex].text,
                  style: TextStyle(color: Colors.white, fontSize: 25.0),
                )),
              ),
              back: Container(
                margin: EdgeInsets.all(5),
                width: double.infinity,
                height: 150,
                color: Colors.red.shade600,
                child: Center(
                    child: Text(
                  getCorrectAnswer(_questions[cindex]),
                  style: TextStyle(color: Colors.white, fontSize: 25.0),
                )),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ..._questions[cindex].answers.map((e) {
                  return ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          backgroundColor:
                              (getCorrectAnswer(_questions[cindex]) == e.text &&
                                      e.text == _c_answer)
                                  ? Colors.green
                                  : Colors.red),
                      onPressed: () {
                        if (respond_allowed) {
                          respond_allowed = false;
                          _c_answer = e.text;
                          _must_flip = true;
                          if (getCorrectAnswer(_questions[cindex]) == e.text) {
                            _u_score += e.Score;
                          }

                          setState(() {});
                        }
                      },
                      child: Text(e.text));
                }).toList(),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Card(
              elevation: 10,
              color: Colors.orange.shade100,
              child: Container(
                padding: EdgeInsets.all(10),
                width: double.infinity,
                height: 150,
                child: Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: CircleAvatar(
                        radius: 50,
                        backgroundColor: Colors.blue.shade300,
                      ),
                    ),
                    Column(
                      children: [
                        Text(
                          "Ahmed Alin Jama",
                          style: TextStyle(fontSize: 20),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Wrap(children: [
                          Text(
                              "Ahmed wuxuu isticmaalaaa adeega 4G iyo internet ka"),
                        ]),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
