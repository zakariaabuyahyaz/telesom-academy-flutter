import 'dart:convert';

import 'package:ascworld/ExampleOne.dart';
import 'package:ascworld/FormExamples.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:http/http.dart' as http;
import 'package:ascworld/api_links.dart';

class EmployeeList extends StatefulWidget {
  const EmployeeList({super.key});

  @override
  State<EmployeeList> createState() => _EmployeeListState();
}

class _EmployeeListState extends State<EmployeeList> {
  List<dynamic> _employees = [];

  Future<void> fetchdata() async {
    var response = await http.get(Uri.parse(api_links.fetch_url));
    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      _employees = data;
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    fetchdata().then((value) {
      setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        title: Text('Employee List'),
        backgroundColor: Colors.teal,
        actions: [
          PopupMenuButton(
            onSelected: (value) {
              if (value == 'new') {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => FromExamples(),
                  ),
                );
              }
            },
            itemBuilder: (context) {
              return [
                PopupMenuItem(
                  child: Text('New'),
                  value: 'new',
                )
              ];
            },
          )
        ],
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
        child: RefreshIndicator(
          onRefresh: () async {
            print('d...');
            fetchdata().then((value) {
              setState(() {});
            });
          },
          child: ListView.builder(
            itemCount: _employees.length,
            itemBuilder: (context, index) {
              return ListTile(
                leading: CircleAvatar(backgroundColor: Colors.grey),
                title: Text(_employees[index]['fullname']),
                subtitle: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text('Email'),
                        SizedBox(
                          width: 15.0,
                        ),
                        Text(_employees[index]['email']),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text('Mobile'),
                        SizedBox(
                          width: 15.0,
                        ),
                        Text(_employees[index]['mobile']),
                      ],
                    ),
                  ],
                ),
                trailing: PopupMenuButton(
                  onSelected: (value) async {
                    if (value == 'delete') {
                      String _m = api_links.delete_url +
                          '/' +
                          _employees[index]["id"].toString();
                      var resp = await http.delete(Uri.parse(_m));
                    }
                  },
                  itemBuilder: (context) {
                    return [
                      PopupMenuItem(
                        child: Text('Delete'),
                        value: 'delete',
                      )
                    ];
                  },
                ),
              );
            },
          ),
        ),
      ),
    ));
  }
}
