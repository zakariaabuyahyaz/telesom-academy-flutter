import 'package:ascworld/Backend/create.dart';
import 'package:ascworld/Backend/list.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class RDrawer extends StatelessWidget {
  const RDrawer({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        DrawerHeader(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            CircleAvatar(
              backgroundColor: Colors.purple,
              radius: 50.0,
            ),
            SizedBox(
              height: 15,
            ),
            Text('Zakaria Mohamed'),
          ],
        )),
        Divider(
          height: 2.0,
        ),
        ListTile(
          leading: Icon(Icons.settings),
          title: Text('Settings'),
          onTap: () {
            Navigator.of(context).pop();
            Navigator.of(context).pushNamed("/settings");
          },
        ),
        ListTile(
          leading: Icon(Icons.info),
          title: Text('Details'),
          onTap: () {
            Navigator.of(context).pop();
            Navigator.of(context).pushNamed("/details");
          },
        ),
        ListTile(
          leading: Icon(Icons.person),
          title: Text('Employee List'),
          onTap: () {
            Navigator.of(context).pop();
            Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => BListEmployee(),
            ));
          },
        ),
        ListTile(
          leading: Icon(Icons.add),
          title: Text('Create Employee'),
          onTap: () {
            Navigator.of(context).pop();
            Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => BCreateEmployee(),
            ));
          },
        ),
      ],
    );
  }
}
