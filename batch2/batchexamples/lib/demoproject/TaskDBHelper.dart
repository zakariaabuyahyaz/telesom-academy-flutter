import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart' as path;
import 'package:batchexamples/demoproject/Task.dart';

class DbHelper {
  static final String _databaseName = "tasks.db";
  static final int _databaseVersion = 1;

  static Future<Database> getDbConnection() async {
    final databasePath = await getDatabasesPath();
    return openDatabase(
      path.join(databasePath, _databaseName),
      onCreate: (db, version) {
        db.execute(
            "CREATE TABLE Tasks (id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT NOT NULL, completed INTEGER DEFAULT 0)");
      },
      onUpgrade: (db, oldVersion, newVersion) {
        //db.execute("ALTER TABLE USERS ADD USERTYPE INTEGER");
        // Handle database schema upgrades if needed
      },
      version: _databaseVersion,
    );
  }

  // CRUD operations:

  // Insert a new task
  static Future<void> insertTask(Task task) async {
    final db = await getDbConnection();
    await db.insert('Tasks', task.toMap());
  }

  // Read all tasks
  static Future<List<Task>> getTasks() async {
    final db = await getDbConnection();
    final List<Map<String, dynamic>> maps = await db.query('Tasks');
    List<Task> _list = List.generate(maps.length, (i) => Task.fromMap(maps[i]));
    return _list;
  }

  // Update a task
  static Future<void> updateTask(Task task) async {
    final db = await getDbConnection();
    await db
        .update('Tasks', task.toMap(), where: 'id = ?', whereArgs: [task.id]);
  }

  // Delete a task
  static Future<void> deleteTask(int id) async {
    final db = await getDbConnection();
    await db.delete('Tasks', where: 'id = ?', whereArgs: [id]);
  }
}
