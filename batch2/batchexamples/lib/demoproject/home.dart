import 'package:batchexamples/demoproject/Task.dart';
import 'package:flutter/material.dart';
import 'package:batchexamples/demoproject/new.dart';
import 'package:batchexamples/demoproject/TaskDBHelper.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  // Dummy data - replace with actual data from your database
  List<Task> tasks = [];

  Future<void> getTasks() async {
    var _list = await DbHelper.getTasks();
    setState(() {
      tasks = _list;
    });
  }

  @override
  void initState() {
    getTasks();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    int completedTasks = tasks.where((task) => task.completed).length;
    int uncompletedTasks = tasks.where((task) => !task.completed).length;

    return Scaffold(
      appBar: AppBar(
        title: const Text('Task Dashboard'),
      ),
      body: Column(
        children: [
          // Dashboard
          Container(
            padding: const EdgeInsets.all(16.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                _buildDashboardCard(
                  'Pending Tasks',
                  uncompletedTasks.toString(),
                  Colors.orange,
                ),
                _buildDashboardCard(
                  'Completed Tasks',
                  completedTasks.toString(),
                  Colors.green,
                ),
              ],
            ),
          ),
          // Task List
          Expanded(
            child: RefreshIndicator(
              onRefresh: getTasks,
              child: ListView.builder(
                itemCount: tasks.length,
                itemBuilder: (context, index) {
                  return _buildTaskCard(tasks[index]);
                },
              ),
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          // Navigate to create task screen
          // Navigator.push(context, MaterialPageRoute(builder: (context) => CreateTaskScreen()));
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => const CreateTaskScreen()),
          );
        },
        child: const Icon(Icons.add),
      ),
    );
  }

  Widget _buildDashboardCard(String title, String count, Color color) {
    return Card(
      elevation: 4,
      child: Container(
        padding: const EdgeInsets.all(16.0),
        width: MediaQuery.of(context).size.width * 0.4,
        child: Column(
          children: [
            Text(
              title,
              style: const TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(height: 8),
            Text(
              count,
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
                color: color,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildTaskCard(Task task) {
    return Card(
      margin: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
      child: ListTile(
        leading: Icon(
          task.completed ? Icons.check_circle : Icons.circle_outlined,
          color: task.completed ? Colors.green : Colors.grey,
        ),
        title: Text(
          task.title,
          style: TextStyle(
            decoration: task.completed ? TextDecoration.lineThrough : null,
          ),
        ),
        onTap: () {
          // Show task details in a bottom sheet
          _showTaskDetails(task);
        },
        trailing: IconButton(
          icon: const Icon(Icons.check),
          onPressed: () {
            setState(() {
              task.completed = !task.completed;
            });
            DbHelper.updateTask(task);
          },
        ),
      ),
    );
  }

  void _showTaskDetails(Task task) {
    showModalBottomSheet(
      context: context,
      builder: (context) {
        return Container(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Task Details',
                style: Theme.of(context).textTheme.headline6,
              ),
              const SizedBox(height: 16),
              Text('ID: ${task.id}'),
              const SizedBox(height: 8),
              Text('Title: ${task.title}'),
              const SizedBox(height: 8),
              Text('Status: ${task.completed ? "Completed" : "Pending"}'),
              const SizedBox(height: 16),
            ],
          ),
        );
      },
    );
  }
}
