import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:batchexamples/taskapp/task.dart';
import 'package:batchexamples/taskapp/TaskDBHelber.dart';

class NewTaskUI extends StatefulWidget {
  const NewTaskUI({super.key});

  @override
  State<NewTaskUI> createState() => _NewTaskUIState();
}

class _NewTaskUIState extends State<NewTaskUI> {
  TextEditingController _title = TextEditingController();
  bool completed = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('New Task')),
      body: Column(
        children: [
          TextFormField(
            controller: _title,
            decoration: InputDecoration(border: OutlineInputBorder()),
          ),
          SizedBox(
            height: 25,
          ),
          CheckboxListTile(
            value: completed,
            onChanged: (value) {
              setState(() {
                completed = value!;
              });
            },
          ),
          SizedBox(
            height: 50,
          ),
          ElevatedButton(
            child: Text('Submit'),
            onPressed: () {
              final newtask =
                  Task(id: 0, title: _title.text, completed: completed);
              DbHelper.insertTask(newtask);
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }
}
