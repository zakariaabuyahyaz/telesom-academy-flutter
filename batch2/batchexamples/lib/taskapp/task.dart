class Task {
  late int id;
  late String title;
  late bool completed;

  Task({required this.id, required this.title, required this.completed});

  // Factory constructor for creating Task objects from Map data (used for database queries)
  factory Task.fromMap(Map<String, dynamic> map) => Task(
      id: map['id'] as int,
      title: map['title'] as String,
      completed: map['completed'] == 1 ? true : false);

  // Map representation of the Task object (used for database inserts and updates)
  Map<String, dynamic> toMap() => {
        'title': title,
        'completed': completed ? 1 : 0, // Convert bool to integer
      };
}
