import 'package:batchexamples/taskapp/newtask.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

import 'package:batchexamples/taskapp/task.dart';
import 'package:batchexamples/taskapp/TaskDBHelber.dart';
import 'package:flutter/material.dart';

class TaskList extends StatefulWidget {
  const TaskList({super.key});

  @override
  State<TaskList> createState() => _TaskListState();
}

class _TaskListState extends State<TaskList> {
  List<Task> data = [];

  Future<void> loaddata() async {
    final list = await DbHelper.getTasks();
    setState(() {
      data = list;
    });
  }

  @override
  void initState() {
    loaddata();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Tasks')),
      body: RefreshIndicator(
        onRefresh: loaddata,
        child: ListView.builder(
          itemCount: data.length,
          itemBuilder: (context, index) {
            final citem = data[index];
            return ListTile(
              title: Text(citem.title),
              trailing: !citem.completed
                  ? Checkbox(
                      value: citem.completed,
                      onChanged: (value) {
                        citem.completed = value!;
                        DbHelper.updateTask(citem);
                        loaddata();
                      },
                    )
                  : null,
            );
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => NewTaskUI(),
            ));
          },
          child: Icon(Icons.add)),
    );
  }
}
