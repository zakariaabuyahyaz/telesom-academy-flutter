import 'dart:convert';

import 'package:batchexamples/CRUD/create.dart';
import 'package:batchexamples/CRUD/employee.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:http/http.dart' as http;

class CList extends StatefulWidget {
  const CList({super.key});

  @override
  State<CList> createState() => _CListState();
}

class _CListState extends State<CList> {
  List data = [];

  Future<void> deleteEmployee(int id) async {
    //write code for deletion
    String URL =
        "http://109.199.125.207/academy_employee_api/public/index.php/api/employees/delete/";
    URL = URL + id.toString();

    var response = await http.get(Uri.parse(URL));
    if (response.statusCode == 200) {
      LoadData();
    }
  }

  Future<void> LoadData() async {
    String URL =
        "http://109.199.125.207/academy_employee_api/public/index.php/api/employees";
    var response = await http.get(Uri.parse(URL));
    if (response.statusCode == 200) {
      var result = jsonDecode(response.body);
      setState(() {
        data = result;
      });
    }
  }

  @override
  void initState() {
    LoadData();
    // TODO: implement initState
    super.initState();
  }

  void updateEmployee(Map<String, dynamic> data) {
    var emp = CEmployee(
      id: data["id"],
      fullname: data["fullname"],
      mother: data["mother"],
      mobile: data["mobile"],
      email: data["email"],
      gender: data["gender"],
      country: data["country"],
      password: data["password"],
      policy: data["is_policyaccepted"] == 0 ? false : true,
    );

    Navigator.of(context).push(MaterialPageRoute(
      builder: (context) => CCreate(
        emp: emp,
      ),
    ));
  }

  Widget buildRow(IconData icon, String Label, String Value) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Icon(icon, size: 12, color: Colors.purple),
          SizedBox(width: 5),
          Text(Label, style: TextStyle(color: Colors.black, fontSize: 13)),
          SizedBox(width: 5),
          Text(Value,
              style: TextStyle(color: Colors.grey.shade600, fontSize: 12)),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: Icon(
          Icons.add,
          size: 20,
          color: Colors.white,
        ),
        onPressed: () {
          Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => CCreate(),
          ));
        },
      ),
      appBar: AppBar(
        title: Text('All Employees'),
      ),
      body: RefreshIndicator(
        onRefresh: () => LoadData(),
        child: ListView.builder(
          itemCount: data.length,
          itemBuilder: (context, index) {
            var citem = data[index];
            return Padding(
              padding: const EdgeInsets.symmetric(vertical: 6),
              child: ListTile(
                  onTap: () {
                    updateEmployee(citem);
                  },
                  title: Text(
                    citem["fullname"],
                    style: TextStyle(fontSize: 16, color: Colors.black),
                  ),
                  subtitle: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      buildRow(Icons.person, "Mother:", citem["mother"]),
                      buildRow(Icons.phone, "Mobile:", citem["mobile"]),
                      buildRow(Icons.email, "Email:", citem["email"]),
                    ],
                  ),
                  trailing: IconButton(
                    icon: Icon(Icons.delete),
                    onPressed: () {
                      showDialog(
                        context: context,
                        builder: (context) {
                          return AlertDialog(
                            title: Text("Confirm?"),
                            content: Text("Do you want to delete?"),
                            actions: [
                              TextButton(
                                onPressed: () {
                                  Navigator.of(context).pop(false);
                                },
                                child: Text("No"),
                              ),
                              TextButton(
                                onPressed: () {
                                  Navigator.of(context).pop(true);
                                },
                                child: Text("Yes"),
                              ),
                            ],
                          );
                        },
                      ).then(
                        (value) {
                          if (value) {
                            deleteEmployee(citem["id"]);
                          }
                        },
                      );
                    },
                  )),
            );
          },
        ),
      ),
    );
  }
}
