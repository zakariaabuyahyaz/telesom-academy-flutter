import 'dart:convert';

import 'package:batchexamples/CRUD/employee.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:http/http.dart' as http;

class CCreate extends StatefulWidget {
  final CEmployee? emp;
  const CCreate({this.emp, super.key});

  @override
  State<CCreate> createState() => _CCreateState();
}

class _CCreateState extends State<CCreate> {
  String fname = "";
  String mother = "";
  String mobile = "";
  String email = "";
  String password = "";
  String country = "";
  String gender = "";
  bool policy = false;

  TextEditingController fullnameController = TextEditingController();

  var _fkey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    String state = "Create";
    if (widget.emp != null) {
      state = "Edit";
      fullnameController.text = widget.emp!.fullname;
    }
    return Scaffold(
      appBar: AppBar(
        title: Text(state == "Create" ? 'Create Employee' : 'Edit Employee'),
      ),
      body: Form(
        autovalidateMode: AutovalidateMode.always,
        key: _fkey,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
          child: ListView(children: [
            TextFormField(
              controller: fullnameController,
              onSaved: (newValue) {
                fname = newValue!;
              },
              validator: (value) {
                if (value == "") {
                  return "Fullname is required";
                }

                if (value!.length < 10) {
                  return "Invalid name";
                }

                return null;
              },
              keyboardType: TextInputType.name,
              maxLength: 20,
              maxLines: 1,
              decoration: InputDecoration(
                contentPadding:
                    EdgeInsets.symmetric(horizontal: 10, vertical: 8),
                border: OutlineInputBorder(),
                labelText: "Fullname",
                hintText: "Enter Firstname, Secondname then Thirdname",
                prefixIcon: Icon(Icons.person),
                floatingLabelBehavior: FloatingLabelBehavior.auto,
              ),
            ),
            SizedBox(
              height: 10,
            ),
            TextFormField(
              onSaved: (newValue) {
                mother = newValue!;
              },
              validator: (value) {
                if (value == "") {
                  return "Mother is required";
                }

                if (value!.length < 10) {
                  return "Invalid mother name";
                }

                return null;
              },
              keyboardType: TextInputType.name,
              maxLength: 20,
              maxLines: 1,
              decoration: InputDecoration(
                contentPadding:
                    EdgeInsets.symmetric(horizontal: 10, vertical: 8),
                border: OutlineInputBorder(),
                labelText: "Mother",
                hintText: "Enter mohter name",
                prefixIcon: Icon(Icons.person),
                floatingLabelBehavior: FloatingLabelBehavior.auto,
              ),
            ),
            SizedBox(
              height: 10,
            ),
            TextFormField(
              onSaved: (newValue) {
                mobile = newValue!;
              },
              validator: (value) {
                if (value == "") {
                  return "Mobile is required";
                }

                if (value!.length < 9) {
                  return "Invalid mobile";
                }

                return null;
              },
              keyboardType: TextInputType.phone,
              maxLength: 20,
              maxLines: 1,
              decoration: InputDecoration(
                contentPadding:
                    EdgeInsets.symmetric(horizontal: 10, vertical: 8),
                border: OutlineInputBorder(),
                labelText: "Mobile",
                hintText: "+25263xxxxxxx",
                prefixIcon: Icon(Icons.phone),
                floatingLabelBehavior: FloatingLabelBehavior.auto,
              ),
            ),
            SizedBox(
              height: 10,
            ),
            TextFormField(
              onSaved: (newValue) {
                email = newValue!;
              },
              validator: (value) {
                if (value == "") {
                  return "email is required";
                }
                return null;
              },
              keyboardType: TextInputType.emailAddress,
              maxLength: 20,
              maxLines: 1,
              decoration: InputDecoration(
                contentPadding:
                    EdgeInsets.symmetric(horizontal: 10, vertical: 8),
                border: OutlineInputBorder(),
                labelText: "Email",
                hintText: "abcd@gmail.com ",
                prefixIcon: Icon(Icons.email),
                floatingLabelBehavior: FloatingLabelBehavior.auto,
              ),
            ),
            SizedBox(
              height: 10,
            ),
            TextFormField(
              onSaved: (newValue) {
                password = newValue!;
              },
              validator: (value) {
                if (value == "") {
                  return "Password is required";
                }

                if (value!.length < 5) {
                  return "Invalid password";
                }

                return null;
              },
              obscureText: true,
              keyboardType: TextInputType.text,
              maxLength: 10,
              maxLines: 1,
              decoration: InputDecoration(
                contentPadding:
                    EdgeInsets.symmetric(horizontal: 10, vertical: 8),
                border: OutlineInputBorder(),
                labelText: "Password",
                hintText: "Enter your password",
                prefixIcon: Icon(Icons.lock),
                floatingLabelBehavior: FloatingLabelBehavior.auto,
              ),
            ),
            SizedBox(
              height: 10,
            ),
            DropdownButtonFormField(
              onSaved: (newValue) {
                country = newValue!;
              },
              validator: (value) {
                if (value == "") {
                  return "Country is required";
                }
                return null;
              },
              decoration: InputDecoration(
                labelText: "Country",
                prefixIcon: Icon(Icons.place),
                border: OutlineInputBorder(),
              ),
              value: country,
              items: [
                DropdownMenuItem(
                  child: Text("Select"),
                  value: "",
                ),
                DropdownMenuItem(
                  child: Text("Somaliland"),
                  value: "Somaliland",
                ),
                DropdownMenuItem(
                  child: Text("Somalia"),
                  value: "Somalia",
                ),
                DropdownMenuItem(
                  child: Text("Ethiopia"),
                  value: "Ethiopia",
                ),
              ],
              onChanged: (value) {
                setState(() {
                  country = value!;
                });
              },
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              children: [
                Text("Male:"),
                Radio(
                  value: "Male",
                  groupValue: gender,
                  onChanged: (value) {
                    setState(() {
                      gender = value!;
                    });
                  },
                ),
                SizedBox(
                  width: 5,
                ),
                Text("Female:"),
                Radio(
                  value: "Famale",
                  groupValue: gender,
                  onChanged: (value) {
                    setState(() {
                      gender = value!;
                    });
                  },
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            CheckboxListTile(
              title: Text("Agree Policy?"),
              subtitle: Text(
                  "By checking the checkbox you agree our terms and policy in this applicaiton. pleaase read carfully"),
              value: policy,
              onChanged: (newvalue) {
                setState(() {
                  policy = newvalue!;
                });
              },
            ),
            SizedBox(
              height: 10,
            ),
            ElevatedButton(
                onPressed: () async {
                  if (_fkey!.currentState!.validate()) {
                    _fkey!.currentState!.save();

                    //create confirmation popup
                    showDialog(
                      context: context,
                      builder: (context) {
                        return AlertDialog(
                          title: Text("Confirmation ?"),
                          content: Text("Are you sure you want to submit?"),
                          actions: [
                            TextButton(
                                onPressed: () {
                                  Navigator.of(context).pop(true);
                                },
                                child: Text("Yes")),
                            TextButton(
                                onPressed: () {
                                  Navigator.of(context).pop(false);
                                },
                                child: Text("No")),
                          ],
                        );
                      },
                    ).then((value) async {
                      if (value) {
                        //prepare object then send to api
                        Map<String, dynamic> newobj = {
                          "fullname": fname,
                          "mother": mother,
                          "mobile": mobile,
                          "email": email,
                          "password": password,
                          "country": country,
                          "is_policyaccepted": policy,
                          "gender": gender.toLowerCase()
                        };
                        String jsonString = jsonEncode(newobj);
                        print(jsonString);
                        String URL =
                            "http://109.199.125.207/academy_employee_api/public/index.php/api/employees/create";

                        var resonse = await http.post(
                          Uri.parse(URL),
                          headers: {"content-type": "application/json"},
                          body: jsonString,
                        );
                        print(resonse.body);

                        if (resonse.statusCode == 200) {
                          ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(
                              content: Text("Employee created successfully"),
                              duration: Duration(seconds: 5),
                              backgroundColor: Colors.blue.shade200,
                            ),
                          );
                        } else {
                          ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(
                              content: Text("Operation Failed, try again !!"),
                              duration: Duration(seconds: 5),
                              backgroundColor: Colors.red.shade200,
                            ),
                          );
                        }
                      }
                    });
                  }
                },
                child: Text('Submit Data')),
          ]),
        ),
      ),
    );
  }
}
