class CEmployee {
  late int id;
  late String fullname;
  late String mother;
  late String mobile;
  late String email;
  late String gender;
  late String country;
  late bool policy;
  late String password;

  CEmployee(
      {required this.id,
      required this.fullname,
      required this.mother,
      required this.mobile,
      required this.email,
      required this.gender,
      required this.country,
      required this.policy,
      required this.password});
}
