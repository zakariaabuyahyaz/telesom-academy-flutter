import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:google_fonts/google_fonts.dart';

class Lesson5 extends StatefulWidget {
  const Lesson5({super.key});

  @override
  State<Lesson5> createState() => _Lesson5State();
}

class _Lesson5State extends State<Lesson5> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Lesson 5'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Roboto Font',
              style: TextStyle(fontSize: 24, fontFamily: 'Roboto'),
            ),
            Text(
              'Arial Font',
              style: TextStyle(fontSize: 24, fontFamily: 'Arial'),
            ),
            Text(
              'Courier New Font',
              style: TextStyle(fontSize: 24, fontFamily: 'Courier New'),
            ),
            Text(
              'Times New Roman Font',
              style: TextStyle(fontSize: 24, fontFamily: 'Times New Roman'),
            ),
            Text(
              'Georgia Font',
              style: TextStyle(fontSize: 24, fontFamily: 'Georgia'),
            ),
            Text(
              'This is from google fonts website',
              style: GoogleFonts.spaceGrotesk(
                fontSize: 25,
              ),
            ),
          ],
        ),
      ),
      // body: Center(
      //   child: Text(
      //     'Telesom Academy',
      //     style: TextStyle(fontSize: 25),
      //   ),
      // ),
      // body: Align(
      //   alignment: Alignment(0.6, -0.6),
      //   child: Container(
      //     width: 50,
      //     height: 50,
      //     color: Colors.red,
      //   ),
      // ),
    );
  }
}

class Classwork5 extends StatefulWidget {
  const Classwork5({super.key});

  @override
  State<Classwork5> createState() => _Classwork5State();
}

class _Classwork5State extends State<Classwork5> {
  List<String> items = ['Computer', 'Laptop', 'Mouse', 'Desk'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Class Work 5')),
      body: Column(
        children: items.map((e) {
          return Container(
            padding: EdgeInsets.all(5),
            margin: EdgeInsets.all(5),
            width: 100,
            height: 100,
            color: Colors.blue.shade100,
            child: Center(
              child: Text(e),
            ),
          );
        }).toList(),
      ),
    );
  }
}
