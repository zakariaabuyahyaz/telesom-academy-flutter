import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class Calculator extends StatefulWidget {
  const Calculator({super.key});

  @override
  State<Calculator> createState() => _CalculatorState();
}

class _CalculatorState extends State<Calculator> {
  String displayText = '0';
  double firstoperand = 0.0;
  double secondoperand = 0.0;
  String operator = '';
  double result = 0.0;

  Widget _buildButton(String text) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: ElevatedButton(
        onPressed: () => clickHandler(text),
        child: Text(text),
      ),
    );
  }

  void clickHandler(String text) {
    setState(() {
      //check if its numeric
      List<String> nums = [
        '0',
        '1',
        '2',
        '3',
        '4',
        '5',
        '6',
        '7',
        '8',
        '8',
        '9'
      ];
      if (nums.contains(text)) {
        if (displayText == '0') {
          displayText = text;
        } else {
          displayText += text;
        }
      } else if (text == "+" || text == "-" || text == "*" || text == "/") {
        firstoperand = double.parse(displayText);
        displayText = '';
        operator = text;
      } else if (text == "C") {
        displayText = '';
        firstoperand = 0.0;
        secondoperand = 0.0;
        result = 0.0;
      } else if (text == "=") {
        secondoperand = double.parse(displayText);
        displayText = '';
        switch (operator) {
          case "+":
            result = firstoperand + secondoperand;
            break;
          case "-":
            result = firstoperand - secondoperand;
            break;
          case "*":
            result = firstoperand * secondoperand;
            break;
          case "/":
            result = firstoperand / secondoperand;
            break;
        }
        displayText = result.toString();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Caawiye App')),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Container(
            child: Text(
              displayText,
              style: TextStyle(fontSize: 25, fontWeight: FontWeight.w600),
            ),
            alignment: Alignment.centerRight,
          ),
          SizedBox(
            height: 50,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              _buildButton('7'),
              _buildButton('8'),
              _buildButton('9'),
              _buildButton('+'),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              _buildButton('4'),
              _buildButton('5'),
              _buildButton('6'),
              _buildButton('-'),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              _buildButton('1'),
              _buildButton('2'),
              _buildButton('3'),
              _buildButton('*'),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              _buildButton('0'),
              _buildButton('.'),
              _buildButton('C'),
              _buildButton('/'),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [_buildButton('=')],
          ),
        ],
      ),
    );
  }
}
