import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class Example10 extends StatefulWidget {
  const Example10({super.key});

  @override
  State<Example10> createState() => _Example10State();
}

class _Example10State extends State<Example10> {
  TextEditingController _fullnameController = TextEditingController();
  String fullname = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Forms')),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(children: [
          TextFormField(
            controller: _fullnameController,
            keyboardType: TextInputType.text,
            maxLength: 15,
            maxLines: 1,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 8),
              border: OutlineInputBorder(),
              labelText: "Fullname",
              labelStyle: TextStyle(color: Colors.red),
              hintText: "Enter Firstname, Secondname then Thirdname",
              prefixIcon: Icon(Icons.person),
              floatingLabelBehavior: FloatingLabelBehavior.auto,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          TextFormField(),
          OutlinedButton(
              onPressed: () {
                _fullnameController.text = "Flutter";
                // setState(() {
                //   fullname = _fullnameController.text;
                // });
              },
              child: Text('Read')),
          SizedBox(
            height: 10,
          ),
          Text(fullname),
        ]),
      ),
    );
  }
}
