import 'dart:convert';

import 'package:batchexamples/Forms/api_links.dart';
import 'package:batchexamples/Forms/create.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:http/http.dart' as http;

class AllEmployees extends StatefulWidget {
  const AllEmployees({super.key});

  @override
  State<AllEmployees> createState() => _AllEmployeesState();
}

class _AllEmployeesState extends State<AllEmployees> {
  List employees = [];

  Future<void> loademployees() async {
    var response = await http.get(Uri.parse(ApiLinks.getList_url));
    if (response.statusCode == 200) {
      print(jsonDecode(response.body));
      employees = jsonDecode(response.body);
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    loademployees().then((value) {
      setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("All Employees"),
        actions: [
          PopupMenuButton(
            itemBuilder: (context) => [
              PopupMenuItem(
                child: Text("New"),
                value: 1,
              ),
              PopupMenuItem(
                child: Text("Delete"),
                value: 2,
              )
            ],
            onSelected: (value) {
              if (value == 1) {
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => Create(),
                ));
              }
            },
          )
        ],
      ),
      body: RefreshIndicator(
        onRefresh: () async {
          loademployees();
          setState(() {});
        },
        child: ListView.builder(
          itemCount: employees.length,
          itemBuilder: (context, index) {
            var citem = employees[index];
            return Padding(
              padding: const EdgeInsets.symmetric(vertical: 10),
              child: ListTile(
                leading: CircleAvatar(
                  backgroundColor: Colors.purple,
                  radius: 25,
                ),
                title: Text(citem['fullname']),
                subtitle: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Mobile: " + citem['mobile']),
                      Text("Email: " + citem['email']),
                    ]),
                trailing: Icon(
                  Icons.menu,
                  size: 30,
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
