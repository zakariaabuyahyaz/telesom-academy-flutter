import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class ThemeTutorial extends StatefulWidget {
  const ThemeTutorial({super.key});

  @override
  State<ThemeTutorial> createState() => _ThemeTutorialState();
}

class _ThemeTutorialState extends State<ThemeTutorial> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Theme Tutorials"),
      ),
      body: Column(children: [
        Text(
          "Display Large",
          style: Theme.of(context).textTheme.displayLarge,
        ),
        Text(
          "Display Medium",
          style: Theme.of(context).textTheme.displayMedium,
        ),
        Text(
          "Display Small",
          style: Theme.of(context).textTheme.displaySmall,
        ),
        SizedBox(
          height: 15,
        ),
        Icon(
          Icons.person,
          color: Theme.of(context).colorScheme.primary,
        ),
        SizedBox(
          height: 15,
        ),
        ElevatedButton(
          onPressed: () {},
          child: Text("Click me"),
          style: ElevatedButton.styleFrom(
            backgroundColor: Theme.of(context).primaryColor,
            textStyle:
                TextStyle(color: Theme.of(context).colorScheme.secondary),
          ),
        ),
      ]),
    );
  }
}
