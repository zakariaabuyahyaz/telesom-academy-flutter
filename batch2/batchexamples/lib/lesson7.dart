import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class Lesson7 extends StatefulWidget {
  const Lesson7({super.key});

  @override
  State<Lesson7> createState() => _Lesson7State();
}

class _Lesson7State extends State<Lesson7> {
  // List<String> inventory = ['Computer', 'Mouse', 'Keyboard', 'Mobile'];
  List<String> inventory = List.generate(100, (index) => "Item Number $index");
  List<facebook_post> data = [
    facebook_post(
        user: "Khalid Omar",
        timestamp: "2024-09-30 14:25:38",
        user_image:
            "https://via.placeholder.com/150/0000FF/FFFFFF/?text=Somaliland+1",
        content:
            "The government has announced new initiatives to boost education in Somaliland. The plans include increased funding for schools and scholarships for students from low-income families. This move aims to provide equal opportunities for all students and enhance the overall quality of education in the region.",
        content_image:
            "https://via.placeholder.com/150/FF0000/FFFFFF/?text=Somaliland+2"),

    facebook_post(
        user: "Amina Mohamed",
        timestamp: "2024-09-25 10:12:45",
        user_image:
            "https://via.placeholder.com/150/00FF00/FFFFFF/?text=Somaliland+3",
        content:
            "Recent developments in the agricultural sector have shown promising results for local farmers. The introduction of new farming techniques and better irrigation methods has significantly increased crop yields. Farmers are optimistic about the upcoming harvest season and its potential impact on food security.",
        content_image:
            "https://via.placeholder.com/150/FFFF00/FFFFFF/?text=Somaliland+4"),

    facebook_post(
        user: "Fatima Yusuf",
        timestamp: "2024-09-20 08:45:22",
        user_image:
            "https://via.placeholder.com/150/FF00FF/FFFFFF/?text=Somaliland+5",
        content:
            "The Somaliland tourism industry is witnessing a revival as more international tourists visit the region. Efforts to promote Somaliland's rich cultural heritage and natural beauty have paid off. Tour operators are offering unique experiences that showcase the history and diversity of the region.",
        content_image:
            "https://via.placeholder.com/150/00FFFF/FFFFFF/?text=Somaliland+6"),

    facebook_post(
        user: "Mohamed Noor",
        timestamp: "2024-09-18 16:20:55",
        user_image:
            "https://via.placeholder.com/150/000000/FFFFFF/?text=Somaliland+7",
        content:
            "A new health clinic has opened in Hargeisa, providing essential medical services to underserved communities. The clinic aims to improve healthcare access and quality for residents. Health officials emphasize the importance of preventative care and community health education.",
        content_image:
            "https://via.placeholder.com/150/FFFFFF/000000/?text=Somaliland+8"),

    facebook_post(
        user: "Abdi Ahmed",
        timestamp: "2024-09-15 11:30:12",
        user_image:
            "https://via.placeholder.com/150/FFA500/FFFFFF/?text=Somaliland+9",
        content:
            "The youth of Somaliland are taking the lead in various community projects aimed at improving local infrastructure. From clean-up campaigns to building recreational facilities, these initiatives reflect the dedication of the younger generation to creating a better environment for everyone.",
        content_image:
            "https://via.placeholder.com/150/800080/FFFFFF/?text=Somaliland+10"),

    facebook_post(
        user: "Sadia Ismail",
        timestamp: "2024-09-28 13:15:34",
        user_image:
            "https://via.placeholder.com/150/0000FF/FFFFFF/?text=Somaliland+1",
        content:
            "Somaliland's sports teams have been performing exceptionally well in recent regional competitions. The athletes' hard work and determination are paying off as they bring home trophies and medals. The community is rallying behind the teams, celebrating their achievements.",
        content_image:
            "https://via.placeholder.com/150/FF0000/FFFFFF/?text=Somaliland+2"),

    facebook_post(
        user: "Hassan Ali",
        timestamp: "2024-09-24 09:45:29",
        user_image:
            "https://via.placeholder.com/150/00FF00/FFFFFF/?text=Somaliland+3",
        content:
            "In response to climate change, Somaliland is investing in renewable energy projects. Solar power initiatives are being implemented to provide sustainable electricity to rural areas. This transition is expected to reduce dependency on fossil fuels and promote environmental conservation.",
        content_image:
            "https://via.placeholder.com/150/FFFF00/FFFFFF/?text=Somaliland+4"),

    facebook_post(
        user: "Layla Ibrahim",
        timestamp: "2024-09-22 07:30:18",
        user_image:
            "https://via.placeholder.com/150/FF00FF/FFFFFF/?text=Somaliland+5",
        content:
            "A cultural festival is set to take place in Hargeisa, showcasing Somaliland's diverse traditions and arts. Local artisans and performers will gather to celebrate their heritage through music, dance, and crafts. This event aims to foster unity and pride among the people.",
        content_image:
            "https://via.placeholder.com/150/00FFFF/FFFFFF/?text=Somaliland+6"),

    facebook_post(
        user: "Khalid Omar",
        timestamp: "2024-09-17 15:20:47",
        user_image:
            "https://via.placeholder.com/150/000000/FFFFFF/?text=Somaliland+7",
        content:
            "The Somaliland government has initiated discussions with neighboring countries to enhance trade relations. The aim is to create a more prosperous region through increased economic collaboration. Experts believe this could lead to significant growth and job creation.",
        content_image:
            "https://via.placeholder.com/150/FFFFFF/000000/?text=Somaliland+8"),

    facebook_post(
        user: "Amina Mohamed",
        timestamp: "2024-09-14 12:10:38",
        user_image:
            "https://via.placeholder.com/150/FFA500/FFFFFF/?text=Somaliland+9",
        content:
            "Environmental activists are raising awareness about the importance of conserving Somaliland's natural resources. Campaigns are being organized to educate the public about sustainable practices and the impacts of pollution. Community involvement is key to these efforts.",
        content_image:
            "https://via.placeholder.com/150/800080/FFFFFF/?text=Somaliland+10"),

    // Additional posts...
    facebook_post(
        user: "Fatima Yusuf",
        timestamp: "2024-09-13 09:55:16",
        user_image:
            "https://via.placeholder.com/150/0000FF/FFFFFF/?text=Somaliland+1",
        content:
            "The Somaliland government has launched a new initiative to support local artisans and craftsmen. This program aims to promote traditional crafts and help artisans market their products effectively. By providing training and resources, the government hopes to boost the local economy.",
        content_image:
            "https://via.placeholder.com/150/FF0000/FFFFFF/?text=Somaliland+2"),

    facebook_post(
        user: "Mohamed Noor",
        timestamp: "2024-09-12 11:02:45",
        user_image:
            "https://via.placeholder.com/150/00FF00/FFFFFF/?text=Somaliland+3",
        content:
            "Somaliland is enhancing its digital infrastructure to improve connectivity across the region. The initiative includes expanding internet access in rural areas, which is expected to foster economic growth and educational opportunities for residents.",
        content_image:
            "https://via.placeholder.com/150/FFFF00/FFFFFF/?text=Somaliland+4"),

    facebook_post(
        user: "Abdi Ahmed",
        timestamp: "2024-09-11 14:35:22",
        user_image:
            "https://via.placeholder.com/150/FF00FF/FFFFFF/?text=Somaliland+5",
        content:
            "A major investment is being made in the Somaliland fishing industry, aimed at increasing production and sustainability. This project is expected to create jobs and provide fresh seafood to local markets, contributing to the economy.",
        content_image:
            "https://via.placeholder.com/150/00FFFF/FFFFFF/?text=Somaliland+6"),

    facebook_post(
        user: "Sadia Ismail",
        timestamp: "2024-09-10 08:20:15",
        user_image:
            "https://via.placeholder.com/150/000000/FFFFFF/?text=Somaliland+7",
        content:
            "The Somaliland Ministry of Culture has announced plans to preserve and promote traditional music and dance forms. Programs will be established to train young artists and encourage cultural exchange, fostering appreciation for Somaliland's rich heritage.",
        content_image:
            "https://via.placeholder.com/150/FFFFFF/000000/?text=Somaliland+8"),

    facebook_post(
        user: "Hassan Ali",
        timestamp: "2024-09-09 17:50:33",
        user_image:
            "https://via.placeholder.com/150/FFA500/FFFFFF/?text=Somaliland+9",
        content:
            "In a recent seminar, experts discussed the challenges and opportunities in Somaliland's healthcare sector. Emphasis was placed on the need for better access to medical services and the importance of community health initiatives.",
        content_image:
            "https://via.placeholder.com/150/800080/FFFFFF/?text=Somaliland+10"),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Lesson 7')),
      // body: ListView(children: [
      //   Container(
      //     width: 150,
      //     height: 150,
      //     color: Colors.red,
      //   ),
      //   SizedBox(
      //     height: 10,
      //   ),
      //   Container(
      //     width: 150,
      //     height: 800,
      //     color: Colors.blue,
      //   ),
      //   SizedBox(
      //     height: 10,
      //   ),
      //   Container(
      //     width: 150,
      //     height: 150,
      //     color: Colors.yellow,
      //   ),
      // ]),
      // body: ListView.builder(
      //   itemCount: inventory.length,
      //   itemBuilder: (context, index) {
      //     return Text(inventory[index]);
      //   },
      // ),
      // body: Column(children: [
      //   ListTile(
      //     leading: CircleAvatar(
      //       radius: 30,
      //       backgroundColor: Colors.blue,
      //     ),
      //     trailing: Icon(Icons.menu_rounded),
      //     title: Text(
      //       'Khaalid Foodhaadhi',
      //       style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
      //     ),
      //     subtitle: Text(
      //       'Last seen 20 min ago',
      //       style: TextStyle(color: Colors.grey, fontSize: 13),
      //     ),
      //   )
      // ]),
      body: ListView.builder(
        itemCount: data.length,
        itemBuilder: (context, index) {
          facebook_post _current_post = data[index];
          return ListTile(
            leading: CircleAvatar(
              radius: 15,
              backgroundImage: NetworkImage(_current_post.user_image),
            ),
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  _current_post.user,
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 3,
                ),
                Text(
                  _current_post.timestamp,
                  style: TextStyle(fontSize: 13, color: Colors.grey),
                ),
              ],
            ),
            subtitle: Column(
              children: [
                Text(
                  _current_post.content,
                  style: TextStyle(fontSize: 13, color: Colors.black),
                  textAlign: TextAlign.justify,
                ),
                SizedBox(
                  height: 5,
                ),
                Image.network(_current_post.content_image)
              ],
            ),
            trailing: Icon(Icons.menu_outlined),
          );
        },
      ),
    );
  }
}

class facebook_post {
  late String user;
  late String timestamp;
  late String user_image;
  late String content;
  late String content_image;
  facebook_post(
      {required this.user,
      required this.timestamp,
      required this.user_image,
      required this.content,
      required this.content_image});
}
