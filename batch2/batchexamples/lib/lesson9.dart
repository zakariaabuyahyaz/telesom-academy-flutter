import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class Lesson9 extends StatefulWidget {
  const Lesson9({super.key});

  @override
  State<Lesson9> createState() => _Lesson9State();
}

class _Lesson9State extends State<Lesson9> {
  double mywidth = 100;
  double myheight = 100;
  Color mycolor = Colors.blue;
  double myopactity = 1;
  Alignment myalign = Alignment.center;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Lesson 9'),
        ),
        body: Column(
          children: [
            AnimatedContainer(
              duration: Duration(seconds: 5),
              width: mywidth,
              height: myheight,
              color: mycolor,
              child: AnimatedAlign(
                alignment: myalign,
                duration: Duration(seconds: 5),
                child: CircleAvatar(
                  radius: 20,
                ),
              ),
            ),
            SizedBox(height: 10),
            AnimatedOpacity(
              opacity: myopactity,
              duration: Duration(seconds: 5),
              child: Container(
                width: 70,
                height: 70,
                color: Colors.red,
              ),
            ),
            SizedBox(height: 10),
            OutlinedButton(
                onPressed: () {
                  setState(() {
                    mywidth = 200;
                    myheight = 200;
                    mycolor = Colors.red;

                    myopactity = 0.2;
                    myalign = Alignment.topRight;
                  });
                },
                child: Text('Animate')),
          ],
        ));
  }
}
