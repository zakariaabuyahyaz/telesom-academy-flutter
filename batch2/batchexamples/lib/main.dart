import 'package:batchexamples/CRUD/list.dart';
import 'package:batchexamples/Forms/Examples.dart';
import 'package:batchexamples/Forms/create.dart';
import 'package:batchexamples/Forms/list.dart';
import 'package:batchexamples/GlobalState/globalone.dart';
import 'package:batchexamples/GlobalState/mycart.dart';
import 'package:batchexamples/Navigation/dashboard.dart';
import 'package:batchexamples/Navigation/home.dart';
import 'package:batchexamples/Navigation/login.dart';
import 'package:batchexamples/lib/Forms/create_task.dart';
import 'package:batchexamples/themeexample.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:batchexamples/lesson9.dart';
import 'package:batchexamples/BottomNavigation/bottomnav.dart';
import 'package:batchexamples/AppTheme.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:batchexamples/demoproject/splash.dart';
import 'package:get/get.dart';
import 'package:batchexamples/GlobalState/CartState.dart';

void main() {
  runApp(RunApp());
}

class RunApp extends StatefulWidget {
  const RunApp({super.key});

  @override
  State<RunApp> createState() => _RunAppState();
}

class _RunAppState extends State<RunApp> {
  int x = 9;
  var conn = Get.put(MyCart());
  var cart = Get.put(ItemCart());
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: GSHomeOne(),
    );
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: GSHomeOne()
        // home: NHome(),
        // theme: my_light_theme,
        // darkTheme: my_dark_theme,
        // themeMode: ThemeMode.system,
        // initialRoute: "/",
        // routes: {
        //   "/": (context) => TaskListScreen(),
        //   "/login": (context) => NLogin(),
        //   "/dashboard": (context) => NDashboard(
        //         username: "Farah",
        //         age: 25,
        //       ),
        // },
        );
  }
}

class StudentListPage extends StatefulWidget {
  @override
  _StudentListPageState createState() => _StudentListPageState();
}

class _StudentListPageState extends State<StudentListPage> {
  bool isLoading = true;

  List<Map<String, String>> students = [
    {
      'name': 'John Doe',
      'mobile': '252 63 600 001',
      'class': '10',
      'class_no': '1001',
      'image_url': 'https://via.placeholder.com/150'
    },
    {
      'name': 'Jane Smith',
      'mobile': '252 63 600 002',
      'class': '9',
      'class_no': '1002',
      'image_url': 'https://via.placeholder.com/150'
    },
    {
      'name': 'Ahmed Ali',
      'mobile': '252 63 600 003',
      'class': '12',
      'class_no': '1003',
      'image_url': 'https://via.placeholder.com/150'
    },
  ];

  Color mycolor = Colors.blue;
  double mywidth = 100;
  double myheight = 100;
  double myopacity = 1;
  Alignment myalignment = Alignment.center;
  @override
  void initState() {
    //loading data from api
    super.initState();
    // Simulate a delay to mimic loading data
    Future.delayed(Duration(seconds: 3), () {
      setState(() {
        isLoading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Shimmer Effect with Student Data'),
      ),
      body: Column(
        children: [
          AnimatedContainer(
            duration: Duration(seconds: 5),
            color: mycolor,
            width: mywidth,
            height: myheight,
          ),
          SizedBox(
            height: 10,
          ),
          AnimatedOpacity(
            opacity: myopacity,
            duration: Duration(seconds: 2),
            child: Container(
              width: 30,
              height: 30,
              color: Colors.purple,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          AnimatedAlign(
            alignment: myalignment,
            duration: Duration(seconds: 5),
            child: CircleAvatar(
              radius: 30,
              backgroundColor: Colors.black,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          TweenAnimationBuilder(
            tween: Tween<double>(begin: 0.5, end: 1),
            duration: Duration(seconds: 5),
            builder: (context, value, child) {
              return Container(
                color: Colors.amber.shade100,
                width: 100 * value,
                height: 100 * value,
                child: Opacity(
                  opacity: value,
                  child: Text(
                    'Welcome to Flutter',
                    style: TextStyle(fontSize: 15 * value),
                  ),
                ),
              );
            },
          ),
          SizedBox(
            height: 10,
          ),
          OutlinedButton(
              onPressed: () {
                setState(() {
                  mycolor = Colors.red;
                  myheight = 230;
                  mywidth = 230;

                  myopacity = 0.2;
                  myalignment = Alignment.topRight;
                });
              },
              child: Text('Animate')),
        ],
      ),
      // body: RefreshIndicator(
      //     onRefresh: () async {
      //       Future.delayed(Duration(seconds: 1), () {
      //         setState(() {
      //           isLoading = true;
      //         });
      //       });

      //       Future.delayed(Duration(seconds: 5), () {
      //         setState(() {
      //           isLoading = false;
      //         });
      //       });
      //     },
      //     child: isLoading ? buildShimmerList() : buildStudentList()),
    );
  }

  // Shimmer Loading List
  Widget buildShimmerList() {
    return ListView.builder(
      itemCount: 5,
      itemBuilder: (context, index) {
        return Shimmer.fromColors(
          baseColor: Colors.yellow,
          highlightColor: Colors.red,
          child: ListTile(
            leading: CircleAvatar(
              backgroundColor: Colors.white,
              radius: 30,
            ),
            title: Container(
              color: Colors.white,
              height: 20,
              width: double.infinity,
            ),
            subtitle: Container(
              color: Colors.white,
              height: 15,
              width: double.infinity,
            ),
            trailing: Container(
              color: Colors.white,
              height: 20,
              width: 30,
            ),
          ),
        );
      },
    );
  }

  // Actual Student List
  Widget buildStudentList() {
    return ListView.builder(
      itemCount: students.length,
      itemBuilder: (context, index) {
        final student = students[index];
        return ListTile(
          leading: CircleAvatar(
            backgroundImage: NetworkImage(student['image_url']!),
            radius: 30,
          ),
          title: Text(student['name']!),
          subtitle: Text('Mobile: ${student['mobile']}'),
          trailing: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Class: ${student['class']}'),
              Text('No: ${student['class_no']}'),
            ],
          ),
        );
      },
    );
  }
}

class TaskListScreen extends StatelessWidget {
  final List<Map<String, dynamic>> tasks = List.generate(
    10,
    (index) => {
      'id': index + 1,
      'title': 'Task ${index + 1}',
      'description': 'Sample task description',
      'date': '2024-12-01',
      'status': index % 3 == 0 ? 'Completed' : 'Pending',
    },
  );

  @override
  Widget build(BuildContext context) {
    int pendingTasks =
        tasks.where((task) => task['status'] == 'Pending').length;
    int completedTasks =
        tasks.where((task) => task['status'] == 'Completed').length;

    return Scaffold(
      backgroundColor: Colors.grey[100],
      appBar: AppBar(
        title: Text('Task Manager'),
        backgroundColor: Colors.purple,
        elevation: 0,
      ),
      body: Column(
        children: [
          Container(
            padding: EdgeInsets.all(16),
            child: Row(
              children: [
                Expanded(
                  child: Card(
                    elevation: 4,
                    child: Padding(
                      padding: EdgeInsets.all(16),
                      child: Column(
                        children: [
                          Icon(
                            Icons.pending_actions,
                            color: Colors.purple,
                            size: 32,
                          ),
                          SizedBox(height: 8),
                          Text(
                            pendingTasks.toString(),
                            style: TextStyle(
                              fontSize: 24,
                              fontWeight: FontWeight.bold,
                              color: Colors.purple,
                            ),
                          ),
                          Text(
                            'Pending Tasks',
                            style: TextStyle(
                              color: Colors.grey[600],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(width: 16),
                Expanded(
                  child: Card(
                    elevation: 4,
                    child: Padding(
                      padding: EdgeInsets.all(16),
                      child: Column(
                        children: [
                          Icon(
                            Icons.task_alt,
                            color: Colors.purple,
                            size: 32,
                          ),
                          SizedBox(height: 8),
                          Text(
                            completedTasks.toString(),
                            style: TextStyle(
                              fontSize: 24,
                              fontWeight: FontWeight.bold,
                              color: Colors.purple,
                            ),
                          ),
                          Text(
                            'Completed Tasks',
                            style: TextStyle(
                              color: Colors.grey[600],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: ListView.builder(
              padding: EdgeInsets.all(8),
              itemCount: tasks.length,
              itemBuilder: (context, index) {
                final task = tasks[index];
                return Card(
                  elevation: 2,
                  margin: EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                  child: ListTile(
                    leading: CircleAvatar(
                      backgroundColor: Colors.purple[100],
                      child: Text(
                        '${task['id']}',
                        style: TextStyle(
                          color: Colors.purple,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    title: Text(
                      task['title'],
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    subtitle: Text(
                      'Created on: ${task['date']}',
                      style: TextStyle(fontSize: 12),
                    ),
                    trailing: Chip(
                      label: Text(
                        task['status'],
                        style: TextStyle(
                          color: task['status'] == 'Completed'
                              ? Colors.green
                              : Colors.orange,
                          fontSize: 12,
                        ),
                      ),
                      backgroundColor: task['status'] == 'Completed'
                          ? Colors.green[50]
                          : Colors.orange[50],
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          final result = await Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => CreateTaskScreen()),
          );

          if (result != null) {
            // Handle the new task data
            // You'll need to make TaskListScreen stateful to update the list
            // or use state management solution like Provider/Bloc
          }
        },
        backgroundColor: Colors.purple,
        child: Icon(Icons.add),
      ),
    );
  }
}

class SharedExamples extends StatefulWidget {
  const SharedExamples({super.key});

  @override
  State<SharedExamples> createState() => _SharedExamplesState();
}

class _SharedExamplesState extends State<SharedExamples> {
  TextEditingController _controller = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Samples')),
      body: Column(
        children: [
          Text('Shared Preferences Example'),
          TextFormField(
            controller: _controller,
          ),
          ElevatedButton(
            onPressed: () async {
              var prefs = await SharedPreferences.getInstance();
              prefs.setString('name', _controller.text);
            },
            child: Text('Store'),
          ),
          ElevatedButton(
            onPressed: () async {
              var prefs = await SharedPreferences.getInstance();
              prefs.getString('name');
            },
            child: Text('Retrieve'),
          ),
          ElevatedButton(
            onPressed: () async {
              var prefs = await SharedPreferences.getInstance();
              prefs.remove('name');
            },
            child: Text('Delete'),
          ),
        ],
      ),
    );
  }
}
