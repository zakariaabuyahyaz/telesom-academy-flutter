import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class Lesson3 extends StatelessWidget {
  const Lesson3({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Lesson 3'),
      ),
      body: Column(
        children: [
          Text('Buttons Example'),
          SizedBox(
            height: 20,
          ),
          ElevatedButton(
            onPressed: () {
              print('you clicked me');
            },
            style: ElevatedButton.styleFrom(
              backgroundColor: Colors.purple,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(15)),
              ),
            ),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text('Send Email'),
                SizedBox(
                  width: 5,
                ),
                Icon(
                  Icons.send_outlined,
                  color: Colors.white,
                  size: 20,
                ),
              ],
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Icon(
            Icons.person_2_outlined,
            size: 70,
            color: Colors.blue,
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            height: 100,
            width: 100,
            color: Colors.blue,
            margin: EdgeInsets.all(5),
            padding: EdgeInsets.all(5),
            child: Column(
              children: [
                Icon(
                  Icons.attach_money,
                  color: Colors.white,
                  size: 30,
                ),
                Text(
                  'Cash',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  '1,230 USD',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 12,
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: 100,
            width: 100,
            color: Colors.blue,
            margin: EdgeInsets.all(5),
            padding: EdgeInsets.all(5),
            child: Column(
              children: [
                Icon(
                  Icons.sim_card_download_outlined,
                  color: Colors.white,
                  size: 30,
                ),
                Text(
                  'Simcards',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  '250 Pcs',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 12,
                  ),
                ),
              ],
            ),
          ),
          Row(
            children: [
              ElevatedButton(
                onPressed: () {
                  print('you clicked me');
                },
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.purple,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(15)),
                  ),
                ),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text('Increment'),
                    SizedBox(
                      width: 5,
                    ),
                    Icon(
                      Icons.add,
                      color: Colors.white,
                      size: 20,
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: 20,
              ),
              Text(''),
            ],
          ),
        ],
      ),
    );
  }
}

// class Lesson4 extends StatefulWidget {
//   const Lesson4({super.key});

//   @override
//   State<Lesson4> createState() => _Lesson4State();
// }

// class _Lesson4State extends State<Lesson4> {
//   int counter = 1;

//   @override
//   Widget build(BuildContext context) {
//     print('executed');
//     return Scaffold(
//       appBar: AppBar(
//         title: Text('Lesson 3'),
//       ),
//       body: Column(
//         children: [
//           Text('Buttons Example'),
//           SizedBox(
//             height: 20,
//           ),
//           ElevatedButton(
//             onPressed: () {
//               print('you clicked me');
//             },
//             style: ElevatedButton.styleFrom(
//               backgroundColor: Colors.purple,
//               shape: RoundedRectangleBorder(
//                 borderRadius: BorderRadius.all(Radius.circular(15)),
//               ),
//             ),
//             child: Row(
//               mainAxisSize: MainAxisSize.min,
//               children: [
//                 Text('Send Email'),
//                 SizedBox(
//                   width: 5,
//                 ),
//                 Icon(
//                   Icons.send_outlined,
//                   color: Colors.white,
//                   size: 20,
//                 ),
//               ],
//             ),
//           ),
//           SizedBox(
//             height: 20,
//           ),
//           Icon(
//             Icons.person_2_outlined,
//             size: 70,
//             color: Colors.blue,
//           ),
//           SizedBox(
//             height: 10,
//           ),
//           Container(
//             height: 100,
//             width: 100,
//             color: Colors.blue,
//             margin: EdgeInsets.all(5),
//             padding: EdgeInsets.all(5),
//             child: Column(
//               children: [
//                 Icon(
//                   Icons.attach_money,
//                   color: Colors.white,
//                   size: 30,
//                 ),
//                 Text(
//                   'Cash',
//                   style: TextStyle(
//                     color: Colors.white,
//                     fontSize: 15,
//                     fontWeight: FontWeight.bold,
//                   ),
//                 ),
//                 SizedBox(
//                   height: 10,
//                 ),
//                 Text(
//                   '1,230 USD',
//                   style: TextStyle(
//                     color: Colors.white,
//                     fontSize: 12,
//                   ),
//                 ),
//               ],
//             ),
//           ),
//           Container(
//             height: 100,
//             width: 100,
//             color: Colors.blue,
//             margin: EdgeInsets.all(5),
//             padding: EdgeInsets.all(5),
//             child: Column(
//               children: [
//                 Icon(
//                   Icons.sim_card_download_outlined,
//                   color: Colors.white,
//                   size: 30,
//                 ),
//                 Text(
//                   'Simcards',
//                   style: TextStyle(
//                     color: Colors.white,
//                     fontSize: 15,
//                     fontWeight: FontWeight.bold,
//                   ),
//                 ),
//                 SizedBox(
//                   height: 10,
//                 ),
//                 Text(
//                   '250 Pcs',
//                   style: TextStyle(
//                     color: Colors.white,
//                     fontSize: 12,
//                   ),
//                 ),
//               ],
//             ),
//           ),
//           Row(
//             children: [
//               ElevatedButton(
//                 onPressed: () {
//                   // print('inc');
//                   setState(() {
//                     counter++;
//                   });
//                 },
//                 style: ElevatedButton.styleFrom(
//                   backgroundColor: Colors.purple,
//                   shape: RoundedRectangleBorder(
//                     borderRadius: BorderRadius.all(Radius.circular(15)),
//                   ),
//                 ),
//                 child: Row(
//                   mainAxisSize: MainAxisSize.min,
//                   children: [
//                     Text('Increment'),
//                     SizedBox(
//                       width: 5,
//                     ),
//                     Icon(
//                       Icons.add,
//                       color: Colors.white,
//                       size: 20,
//                     ),
//                   ],
//                 ),
//               ),
//               SizedBox(
//                 width: 20,
//               ),
//               Text('$counter'),
//             ],
//           ),
//         ],
//       ),
//     );
//   }
// }
