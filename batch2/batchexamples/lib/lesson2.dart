import 'dart:html';

import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class Lesson2 extends StatelessWidget {
  const Lesson2({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                // margin: EdgeInsets.all(10),
                padding: EdgeInsets.all(5),
                alignment: Alignment.center,
                child: Text('ONE'),
                width: 100,
                height: 100,
                color: Colors.red,
              ),
              Container(
                child: Text('TWO'),
                width: 100,
                height: 100,
                color: Colors.blue,
              ),
              Column(
                children: [
                  Container(
                    width: 30,
                    height: 30,
                    color: Colors.yellow,
                  ),
                  Container(
                    width: 30,
                    height: 30,
                    color: Colors.purple,
                  ),
                ],
              ),
            ],
          ),
          Text('Flutter App'),
        ],
      ),
    );
  }
}

class claswork1 extends StatelessWidget {
  const claswork1({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Classwork 1'),
      ),
      body: Column(
        children: [
          Text(
            'Telesom Academy',
            style: TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.bold,
              color: Colors.grey,
            ),
          ),
          SizedBox(
            height: 100,
          ),
          Text(
            'Telesom Academy is new academy  Academy is new academy  Academy is new academy  Academy is new academy  Academy is new academy  Academy is new academy  Academy is new academy  Academy is new academy  Academy is new academy  Academy is new academy  Academy is new academy  Academy is new academy  Academy is new academy  Academy is new academy ...',
            textAlign: TextAlign.justify,
          ),
        ],
      ),
    );
  }
}
