import 'package:batchexamples/GlobalState/CartState.dart';
import 'package:batchexamples/GlobalState/mycart.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:get/get.dart';

class GSHomeTwo extends StatefulWidget {
  const GSHomeTwo({super.key});

  @override
  State<GSHomeTwo> createState() => _GSHomeTwoState();
}

class _GSHomeTwoState extends State<GSHomeTwo> {
  var mystate = Get.find<MyCart>();
  var mycart = Get.find<ItemCart>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Check Out")),
        body: Obx(
          () => Column(children: [
            ...mycart.items.value.map((e) {
              return Card(
                elevation: 5,
                margin: EdgeInsets.all(5),
                child: Container(
                    padding: EdgeInsets.all(5),
                    color: Colors.purple.shade100,
                    height: 50,
                    width: double.infinity,
                    child: Text(
                      e,
                      style: TextStyle(
                        fontSize: 30,
                      ),
                    )),
              );
            }).toList(),
            SizedBox(
              height: 50,
            ),
            ElevatedButton.icon(
                onPressed: mycart.removeAll,
                icon: Icon(Icons.remove),
                label: Text("Remove All")),
          ]),
          // body: Column(children: [
          //   Obx(() => Text("Counter value is: " + mystate.count.value.toString())),
          //   ElevatedButton(
          //       onPressed: mystate.incrementCount, child: Text("Increment")),
          //   ElevatedButton(
          //       onPressed: mystate.decrementCount, child: Text("Decrement")),
          // ]),,
        ));
  }
}
