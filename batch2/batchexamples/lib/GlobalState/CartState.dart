import 'package:get/get.dart';

class MyCart extends GetxController {
  var count = 0.obs;

  void incrementCount() {
    count.value++;
  }

  void decrementCount() {
    count--;
  }

  void setCount(int x) {
    count.value = x;
  }
}
