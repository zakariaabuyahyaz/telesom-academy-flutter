import 'package:batchexamples/GlobalState/globaltwo.dart';
import 'package:batchexamples/GlobalState/mycart.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:get/get.dart';
import 'package:batchexamples/GlobalState/CartState.dart';

class GSHomeOne extends StatefulWidget {
  const GSHomeOne({super.key});

  @override
  State<GSHomeOne> createState() => _GSHomeOneState();
}

class _GSHomeOneState extends State<GSHomeOne> {
  final mystate = Get.find<MyCart>();
  final mycart = Get.find<ItemCart>();

  final products = ["Computer", "Mobile", "Mouse", "Chair", "Table", "Pen"];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Product List"),
          actions: [
            Obx(
              () => Text(
                mycart.items.value.length.toString(),
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
            ),
            Text("Items"),
            PopupMenuButton(
              itemBuilder: (context) => [
                PopupMenuItem(
                  child: Text("Check Out"),
                  value: 1,
                ),
                PopupMenuItem(
                  child: Text("Empty Cart"),
                  value: 2,
                ),
              ],
              onSelected: (value) {
                if (value == 1) {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => GSHomeTwo(),
                      ));
                } else if (value == 2) {
                  mycart.removeAll();
                }
              },
            )
          ],
        ),
        body: ListView.builder(
          itemCount: products.length,
          itemBuilder: (context, index) {
            var cproduct = products[index];
            int counter = index + 1;
            return ListTile(
              leading: Text(counter.toString()),
              title: Text(cproduct),
              trailing: ElevatedButton.icon(
                  onPressed: () {
                    mycart.addToCart(cproduct);
                  },
                  icon: Icon(Icons.add),
                  label: Text("Add")),
            );
          },
        ));
  }
}
