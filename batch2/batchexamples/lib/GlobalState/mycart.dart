import 'package:get/get.dart';

class ItemCart extends GetxController {
  var items = <String>[].obs;

  void addToCart(String itemname) {
    items.add(itemname);
  }

  void removeAll() {
    items.value = [];
  }
}
