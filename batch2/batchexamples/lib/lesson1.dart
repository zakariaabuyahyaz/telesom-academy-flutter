import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class Lesson1 extends StatelessWidget {
  const Lesson1({super.key});

  @override
  Widget build(BuildContext context) {
    //responsible for UI (screen)
    return MaterialApp(
      home: newlesson(),
    );
  }
}

class newlesson extends StatelessWidget {
  const newlesson({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Welcome to Flutter'),
      ),
      body: Text('Here is the body of my app'),
    );
  }
}
