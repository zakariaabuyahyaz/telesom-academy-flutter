import 'package:flutter/material.dart';

var _primary = Colors.blue;
var _secondary = Colors.white;

var my_light_theme = ThemeData(
  brightness: Brightness.light,
  primaryColor: _primary,
  colorScheme: ColorScheme.light(
      primary: _primary,
      secondary: _secondary,
      background: Colors.white,
      surface: Colors.grey.shade300,
      error: Colors.red),
  elevatedButtonTheme: ElevatedButtonThemeData(
    style: ElevatedButton.styleFrom(
        primary: _primary,
        onPrimary: _primary,
        textStyle: TextStyle(color: _secondary)),
  ),
  textTheme: TextTheme(
    displayLarge: TextStyle(fontSize: 25),
    displayMedium: TextStyle(fontSize: 20),
    displaySmall: TextStyle(fontSize: 15),
    bodyLarge: TextStyle(fontSize: 15),
    bodyMedium: TextStyle(fontSize: 12),
    bodySmall: TextStyle(fontSize: 9),
  ),
);

var _primary_dark = Colors.black;
var _secondary_dark = Colors.white;

var my_dark_theme = ThemeData(
  brightness: Brightness.dark,
  primaryColor: _primary_dark,
  colorScheme: ColorScheme.dark(
      primary: _primary_dark,
      secondary: _secondary_dark,
      background: Colors.white,
      surface: Colors.grey.shade300,
      error: Colors.red),
  elevatedButtonTheme: ElevatedButtonThemeData(
    style: ElevatedButton.styleFrom(
        primary: _primary_dark, onPrimary: _primary_dark),
  ),
  textTheme: TextTheme(
    displayLarge: TextStyle(fontSize: 25),
    displayMedium: TextStyle(fontSize: 20),
    displaySmall: TextStyle(fontSize: 15),
    bodyLarge: TextStyle(fontSize: 15),
    bodyMedium: TextStyle(fontSize: 12),
    bodySmall: TextStyle(fontSize: 9),
  ),
);
