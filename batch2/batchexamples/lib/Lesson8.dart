import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class Lesson8 extends StatefulWidget {
  const Lesson8({super.key});

  @override
  State<Lesson8> createState() => _Lesson8State();
}

class _Lesson8State extends State<Lesson8> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    String s = size.width.toString() + ", " + size.height.toString();

    return Scaffold(
      appBar: AppBar(
        title: Text('Lesson 8'),
        centerTitle: true,
        actions: [
          PopupMenuButton(
            onSelected: (value) {
              print('you selected $value');
            },
            itemBuilder: (context) => [
              PopupMenuItem(child: Text('Save'), value: 1),
              PopupMenuItem(child: Text('Edit'), value: 2),
              PopupMenuItem(child: Text('Delete'), value: 3),
            ],
          ),
        ],
      ),
      // body: GridView.builder(
      //   gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
      //       crossAxisCount: 2, crossAxisSpacing: 10, mainAxisSpacing: 5),
      //   itemCount: 3,
      //   itemBuilder: (context, index) {
      //     return Card(
      //       color: Colors.yellow,
      //       elevation: 5,
      //       child: Center(
      //         child: Text('Item index $index'),
      //       ),
      //     );
      //   },
      // ),
      body: Container(
        width: size.width * 0.8,
        height: size.height * 0.4,
        color: Colors.red,
        child: Center(child: Text(s)),
      ),
    );
  }
}
