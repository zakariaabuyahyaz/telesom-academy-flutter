import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flip_card/flip_card.dart';

class Lesson6 extends StatefulWidget {
  const Lesson6({super.key});

  @override
  State<Lesson6> createState() => _Lesson6State();
}

class _Lesson6State extends State<Lesson6> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Lesson 6')),
      body: Center(
        child: Column(children: [
          Text(
            'Custom Font',
            style: TextStyle(fontSize: 30, fontFamily: 'mycustomfont'),
          ),
          SizedBox(
            height: 10,
          ),
          Card(
            margin: EdgeInsets.all(5),
            color: Colors.pink.shade100,
            shadowColor: Colors.red,
            elevation: 10,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text('Book'),
                  SizedBox(
                    width: 10,
                  ),
                  Icon(Icons.book)
                ],
              ),
            ),
          ),
          SizedBox(
            height: 50,
          ),
          FlipCard(
            flipOnTouch: true,
            direction: FlipDirection.VERTICAL,
            front: Container(
              color: Colors.blue.shade100,
              width: 80,
              height: 180,
              child: Center(child: Text('Front')),
            ),
            back: Container(
              color: Colors.red,
              width: 80,
              height: 180,
              child: Center(child: Text('Back')),
            ),
          )
        ]),
      ),
    );
  }
}
