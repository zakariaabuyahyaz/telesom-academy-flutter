import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class Lesson4 extends StatefulWidget {
  const Lesson4({super.key});

  @override
  State<Lesson4> createState() => _Lesson4State();
}

class _Lesson4State extends State<Lesson4> {
  Color mycolor = Colors.red;
  int a = 150;
  int r = 123;
  int g = 255;
  int b = 100;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Lesson 4')),
      body: Column(
        children: [
          Image(
            image: AssetImage("assets/images/masjid.png"),
            width: 100,
            height: 100,
          ),
          SizedBox(
            height: 50,
          ),
          Image.asset(
            "assets/images/person.jpg",
            width: 100,
            height: 100,
          ),
          Image.network(
            "https://picsum.photos/seed/picsum/200/300",
            width: 100,
            height: 100,
            loadingBuilder: (context, child, loadingProgress) {
              if (loadingProgress == null) {
                return child;
              } else {
                return CircularProgressIndicator();
              }
            },
            errorBuilder: (context, error, stackTrace) {
              return Text('Unable to load image.......');
            },
          ),
          Container(width: 150, height: 150, color: mycolor
              // child: Container(
              //   height: 70,
              //   width: 70,
              //   color: Colors.red,
              // ),
              ),
          ElevatedButton(
            onPressed: () {
              setState(() {
                mycolor = mycolor == Colors.red ? Colors.blue : Colors.red;
              });
            },
            child: Text('Change Color'),
            style: ElevatedButton.styleFrom(
              backgroundColor: mycolor == Colors.red ? Colors.blue : Colors.red,
            ),
          )
        ],
      ),
    );
  }
}

class Exercise4 extends StatefulWidget {
  const Exercise4({super.key});

  @override
  State<Exercise4> createState() => _Exercise4State();
}

class _Exercise4State extends State<Exercise4> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('exercise 4')),
    );
  }
}
