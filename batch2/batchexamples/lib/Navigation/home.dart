import 'package:batchexamples/Navigation/dashboard.dart';
import 'package:batchexamples/Navigation/drawer.dart';
import 'package:batchexamples/Navigation/login.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class NHome extends StatefulWidget {
  const NHome({super.key});

  @override
  State<NHome> createState() => _NHomeState();
}

class _NHomeState extends State<NHome> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text("Home"),
      ),
      drawer: Drawer(
        elevation: 10,
        width: size.width * 0.5,
        backgroundColor: Colors.white,
        child: AppDrawer(),
      ),
      body: SafeArea(
        child: Center(
            child: Column(
          children: [
            OutlinedButton.icon(
                onPressed: () {
                  // Navigator.of(context).push(MaterialPageRoute(
                  //   builder: (context) => NLogin(),
                  // ));
                  Navigator.of(context).pushNamed("/login");
                },
                icon: Icon(Icons.login),
                label: Text("Login")),
            OutlinedButton.icon(
                onPressed: () {
                  // Navigator.pushReplacement(
                  //   context,
                  //   MaterialPageRoute(
                  //       builder: (context) => NDashboard(
                  //             username: "Ali",
                  //             age: 50,
                  //           )),
                  // );
                  Navigator.of(context).pushNamed("/dashboard");
                },
                icon: Icon(Icons.home),
                label: Text("Dashboard")),
          ],
        )),
      ),
    );
  }
}
