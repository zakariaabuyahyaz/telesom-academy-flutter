import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class NDashboard extends StatefulWidget {
  final String username;
  final int age;
  const NDashboard({super.key, required this.username, required this.age});

  @override
  State<NDashboard> createState() => _NDashboardState();
}

class _NDashboardState extends State<NDashboard> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Dashboard"),
        ),
        body: Column(
          children: [
            Text(
              "Dashboard",
              style: TextStyle(fontSize: 35),
            ),
            Text(
              widget.username,
              style: TextStyle(fontSize: 35),
            ),
            Text(
              widget.age.toString(),
              style: TextStyle(fontSize: 35),
            ),
            SizedBox(
              height: 20,
            ),
            IconButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              icon: Icon(Icons.arrow_circle_left_outlined),
            )
          ],
        ));
  }
}
