import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class AppDrawer extends StatelessWidget {
  const AppDrawer({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        DrawerHeader(
          child: Text("TLS Academy"),
          decoration: BoxDecoration(
            color: Colors.purple.shade100,
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Text("Main Menu"),
        SizedBox(
          height: 10,
        ),
        ListTile(
          leading: Icon(Icons.home),
          title: Text("Home"),
          onTap: () {
            Navigator.of(context).pushNamed("/");
            Navigator.of(context).pop();
          },
        ),
        ListTile(
          leading: Icon(Icons.login),
          title: Text("Login"),
          onTap: () {
            Navigator.of(context).pop();
            Navigator.of(context).pushNamed("/login");
          },
        ),
        ListTile(
          leading: Icon(Icons.dashboard),
          title: Text("Dashboard"),
          onTap: () {
            Navigator.of(context).pop();
            Navigator.of(context).pushNamed("/dashboard");
          },
        ),
      ],
    );
  }
}
