import 'dart:ffi';

import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:tlsacademy/newfeedback.dart';

class S_Feedback extends StatefulWidget {
  const S_Feedback({super.key});

  @override
  State<S_Feedback> createState() => _S_FeedbackState();
}

class _S_FeedbackState extends State<S_Feedback> {
  var data = [
    {
      "user": "Ahmed Omer",
      "date": "2022-01-05",
      "comment":
          "waxaan soo jeedin ahaa in acadmy yada gobolada la gaadhsiyowaxaan soo jeedin ahaa in acadmy yada gobolada la gaadhsiyowaxaan soo jeedin ahaa in acadmy yada gobolada la gaadhsiyo",
      "rate": 2
    },
    {
      "user": "Amina Ali",
      "date": "2024-05-05",
      "comment":
          "waxaan soo jeedin ahaa in acadmy yada gobolada la gaadhsiyo waxaan soo jeedin ahaa in acadmy yada gobolada la gaadhsiyo",
      "rate": 5
    }
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'User Feedback',
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        ),
        backgroundColor: Theme.of(context).primaryColor,
        actions: <Widget>[
          IconButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => NewFeedback(),
                ));
              },
              icon: Icon(
                Icons.add,
                color: Colors.white,
                size: 30,
              ))
        ],
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 20),
        child: ListView.builder(
          itemCount: data.length,
          itemBuilder: (context, index) {
            var citem = data[index];
            return Card(
              elevation: 3,
              color: Colors.white,
              child: ListTile(
                leading: CircleAvatar(
                  radius: 25,
                  backgroundImage: AssetImage("assets/images/avatar.png"),
                ),
                title: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          citem["user"].toString(),
                          style: TextStyle(
                              fontSize: 14.0,
                              fontWeight: FontWeight.bold,
                              color: Colors.black),
                        ),
                        RatingBarIndicator(
                          rating: double.parse(citem["rate"].toString()),
                          itemBuilder: (context, index) => Icon(
                            Icons.star,
                            color: Colors.amber,
                          ),
                          itemCount: 5,
                          itemSize: 15.0,
                          direction: Axis.horizontal,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      citem["date"].toString() + " 23 min ago",
                      style: TextStyle(
                          fontSize: 10.0, color: Colors.grey.shade600),
                    ),
                  ],
                ),
                subtitle: Text(
                  textAlign: TextAlign.justify,
                  citem["comment"].toString(),
                  style: TextStyle(fontSize: 10.0, color: Colors.black),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
