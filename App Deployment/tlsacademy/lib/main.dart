import 'package:flutter/material.dart';
import 'package:tlsacademy/mytheme.dart';
import 'package:tlsacademy/tutorial.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Academy',
      theme: mypurple,
      themeMode: ThemeMode.light,
      home: Tutorial(),
    );
  }
}
