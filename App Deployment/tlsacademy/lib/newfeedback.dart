import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class NewFeedback extends StatefulWidget {
  const NewFeedback({super.key});

  @override
  State<NewFeedback> createState() => _NewFeedbackState();
}

class _NewFeedbackState extends State<NewFeedback> {
  final fkey = GlobalKey<FormState>();
  String fname = '';
  String comment = '';
  String mobile = '';
  String email = '';

  Widget buildtexbox(int index, String label, String hint, IconData icon,
      TextInputType keyboard, bool isPassword, int numoflines) {
    return Padding(
      padding: const EdgeInsets.only(top: 10.0),
      child: TextFormField(
        // controller: _nameController,
        maxLines: numoflines,
        obscureText: isPassword,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.all(10.0),
          border: OutlineInputBorder(),
          labelText: label,
          hintText: hint,
          floatingLabelBehavior: FloatingLabelBehavior.auto,
          prefixIcon: Icon(icon),
        ),
        onSaved: (newValue) {
          if (index == 1) {
            fname = newValue ?? '';
          } else if (index == 2) {
            mobile = newValue ?? '';
          } else if (index == 3) {
            email = newValue ?? '';
          } else if (index == 4) {
            comment = newValue ?? '';
          }
        },
        validator: (value) {
          if (value!.isEmpty) {
            return '$label is required';
          }

          return null;
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Send Feedback',
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        ),
        backgroundColor: Theme.of(context).primaryColor,
      ),
      body: Form(
        key: fkey,
        autovalidateMode: AutovalidateMode.always,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 15, vertical: 20),
          child: ListView(children: [
            buildtexbox(1, 'Fullname', 'Enter your fullname',
                Icons.person_2_outlined, TextInputType.name, false, 1),
            buildtexbox(2, 'Email', 'Enter Email Address', Icons.email_outlined,
                TextInputType.emailAddress, false, 1),
            buildtexbox(3, 'Mobile', 'Enter your mobile', Icons.phone,
                TextInputType.phone, false, 1),
            buildtexbox(4, 'Comment', 'Enter your Comment',
                Icons.comment_outlined, TextInputType.text, false, 5),
            SizedBox(
              height: 100,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Theme.of(context).primaryColor,
                    ),
                    onPressed: () async {
                      //display confirmation box
                      showDialog(
                        context: context,
                        builder: (context) {
                          return AlertDialog(
                            title: Text('Confirmation'),
                            content: Text('Are you sure you want to send?'),
                            actions: [
                              TextButton(
                                onPressed: () {
                                  Navigator.of(context).pop(false);
                                },
                                child: Text('No'),
                              ),
                              TextButton(
                                onPressed: () {
                                  Navigator.of(context).pop(true);
                                },
                                child: Text('Yes'),
                              ),
                            ],
                          );
                        },
                      ).then((value) async {
                        if (value) {
                          // to validate the fields
                          if (fkey.currentState!.validate()) {
                            fkey.currentState!.save();

                            // //prepare the object
                            // Map<String, dynamic> newobj = {
                            //   "fullname": fname,
                            //   "mother": mother,
                            //   "mobile": mobile,
                            //   "email": email,
                            //   "gender": gender,
                            //   "country": country,
                            //   "is_policyaccepted": policy,
                            //   "password": password
                            // };

                            // var jsonString = jsonEncode(newobj);
                            // print(jsonString);
                            // var resp = await http.post(
                            //     Uri.parse(api_links.create_url),
                            //     headers: {'content-type': 'application/json'},
                            //     body: jsonString);

                            // print(resp.body);

                            // if (resp.statusCode == 200) {
                            //   ScaffoldMessenger.of(context).showSnackBar(
                            //     SnackBar(
                            //       content:
                            //           Text('Success fully created employee !!'),
                            //       duration: Duration(seconds: 4),
                            //       backgroundColor: Colors.blue,
                            //     ),
                            //   );

                            //   Future.delayed(
                            //     Duration(seconds: 2),
                            //     () {
                            //       Navigator.of(context).pop();
                            //     },
                            //   );
                            // } else {
                            //   String _e = resp.body;
                            //   ScaffoldMessenger.of(context).showSnackBar(
                            //     SnackBar(
                            //       content: Text('Error, $_e'),
                            //       duration: Duration(seconds: 4),
                            //       backgroundColor: Colors.red,
                            //     ),
                            //   );
                            // }

                            //send the to api
                          }
                        }
                      });
                    },
                    child: Row(
                      children: [
                        Icon(Icons.send_outlined),
                        SizedBox(
                          width: 15.0,
                        ),
                        Text('Submit Data'),
                      ],
                    )),
              ],
            ),
          ]),
        ),
      ),
    );
  }
}
