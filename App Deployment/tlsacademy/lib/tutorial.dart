import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:tlsacademy/mydrawer.dart';
import 'package:tlsacademy/mytheme.dart';
import 'package:tlsacademy/data.dart';

class Tutorial extends StatefulWidget {
  const Tutorial({super.key});

  @override
  State<Tutorial> createState() => _TutorialState();
}

class _TutorialState extends State<Tutorial> {
  final apptheme = mypurple;
  final _t = mytext;
  final _t2 = mytext2;

  List<String> courseslist = ["Flutter Mobile App", "CCNA", "Laravel"];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Telesom Academy',
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        ),
        backgroundColor: Theme.of(context).primaryColor,
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 20),
        child: ListView(children: [
          Center(
            child: CircleAvatar(
              backgroundColor: Colors.transparent,
              radius: 100,
              backgroundImage: AssetImage(
                'assets/images/academy.png',
              ),
            ),
          ),
          SizedBox(
            height: 50,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Introduction',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 18,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                _t,
                textAlign: TextAlign.justify,
                style: TextStyle(
                  color: Colors.grey.shade500,
                  fontSize: 13,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                _t2,
                textAlign: TextAlign.justify,
                style: TextStyle(
                  color: Colors.grey.shade500,
                  fontSize: 13,
                ),
              ),
              SizedBox(
                height: 40,
              ),
              Text(
                'Courses',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 18,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 20,
              ),
              ...courseslist
                  .map((e) => Container(
                        margin: EdgeInsets.all(5),
                        padding: EdgeInsets.all(10),
                        color: Theme.of(context).primaryColor,
                        child: Text(
                          e,
                          style: TextStyle(
                              fontSize: 15,
                              color: Colors.white,
                              fontWeight: FontWeight.bold),
                        ),
                      ))
                  .toList(),
            ],
          ),
        ]),
      ),
      drawer: Drawer(
        backgroundColor: Colors.white,
        elevation: 5,
        width: MediaQuery.of(context).size.width * 0.5,
        child: MyDrawer(),
      ),
    );
  }
}
