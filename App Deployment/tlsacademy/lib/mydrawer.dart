import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:tlsacademy/feedback.dart';

class MyDrawer extends StatelessWidget {
  const MyDrawer({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: EdgeInsets.zero,
      children: [
        DrawerHeader(
          decoration: BoxDecoration(color: Theme.of(context).primaryColor),
          child: Column(
            children: [
              CircleAvatar(
                radius: 40,
              ),
              SizedBox(
                height: 15,
              ),
              Text(
                "Guest User",
                style: TextStyle(
                    fontSize: 15,
                    color: Colors.white,
                    fontWeight: FontWeight.bold),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 15,
        ),
        Text(
          "Main Menu",
          style: TextStyle(
              fontSize: 15, color: Colors.white, fontWeight: FontWeight.bold),
        ),
        Divider(
          height: 3,
          color: Colors.grey.shade400,
        ),
        SizedBox(
          height: 8,
        ),
        ListTile(
          leading: Icon(
            Icons.comment_outlined,
            color: Theme.of(context).primaryColor,
            size: 15,
          ),
          title: Text('Feedback & Review'),
          onTap: () {
            print('herer');
            Navigator.of(context).pop();
            Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => S_Feedback(),
            ));
            //
          },
        ),
      ],
    );
  }
}
